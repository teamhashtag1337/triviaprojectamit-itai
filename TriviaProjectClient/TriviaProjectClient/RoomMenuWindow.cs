﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaProjectClient
{
    public partial class RoomMenuWindow : Form
    {
        private NetworkStream clientStream = Config.socket;

        public RoomMenuWindow()
        {
            InitializeComponent();
            RefreshRoomsList();
        }


        private void RefreshPlayersList(int roomId)
        {
            string message = Protocol.USR_ROOM_PLAYERLIST.ToString() + Utils.ParseIntToXBytes(roomId, 4);

            //Send message

            /*
            byte[] buffer = new ASCIIEncoding().GetBytes(message);
            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
            */

            Utils.SendEncrypted(message);

            Utils.ReadEncrypted();

            //Recieve message
            /*
            buffer = new byte[3];
            int bytesRead = clientStream.Read(buffer, 0, 3);
             * */
            string input = Utils.ReadFromStream(3);

            if (input == Protocol.SRV_ROOM_PLAYERLIST.ToString())
            {
                FetchPlayers();
            }
        }

        private void FetchPlayers()
        {
            lstb_players.Items.Clear();



            //byte[] buffer = new byte[1];

            //Reading number of players

            /*
            int bytesRead = clientStream.Read(buffer, 0, 1);
            string input = new ASCIIEncoding().GetString(buffer);
             * */
            int numPlayers = int.Parse(Utils.ReadFromStream(1));

            lbl_numplayers.Visible = true;
            lbl_numplayers.Text = "Number of players: " + numPlayers;

            for (int i = 0; i < numPlayers; i++)
            {
                //Reading username length

                /*
                buffer = new byte[2];
                bytesRead = clientStream.Read(buffer, 0, 2);
                input = new ASCIIEncoding().GetString(buffer);
                 * */
                int nameLength = int.Parse(Utils.ReadFromStream(2));

                //Reading username
                /*
                buffer = new byte[nameLength];
                bytesRead = clientStream.Read(buffer, 0, nameLength);
                 * */
                string input = Utils.ReadFromStream(nameLength);
                lstb_players.Items.Add(input);
            }
        }


        private void RefreshRoomsList()
        {
            
            string message = Protocol.USR_ROOM_REQUEST.ToString();
            /*
            //Send message
            byte[] buffer = new ASCIIEncoding().GetBytes(message);
            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
            */

            Utils.SendEncrypted(message);

            Utils.ReadEncrypted();

            //Recieve message
            /*buffer = new byte[3];
            int bytesRead = clientStream.Read(buffer, 0, 3);
             * */
            string input = Utils.ReadFromStream(3);

            if (input == Protocol.SRV_ROOM_LIST.ToString())
            {
                FetchRooms();
            }

        }


        private void FetchRooms()
        {
            lstb_rooms.Items.Clear();
            byte[] buffer = new byte[4];

            //Reading number of rooms

            /*
            int bytesRead = clientStream.Read(buffer, 0, 4);
            string input = new ASCIIEncoding().GetString(buffer);
             * */
            int numRooms = int.Parse(Utils.ReadFromStream(4));

            for(int i = 0; i < numRooms; i++)
            {
                lstb_rooms.Items.Add(Room.ReadRoom());
            }
        }

        private void RefreshRoomsList(object sender, EventArgs e)
        {
            RefreshRoomsList();
            lstb_players.Items.Clear();
            lbl_numplayers.Text = "NONE";
            lbl_numplayers.Visible = false;
        }

        private void RoomSelected(object sender, EventArgs e)
        {
            int ind = lstb_rooms.SelectedIndex;
            if(ind != -1)
            {
                Room room = (Room)lstb_rooms.Items[ind];
                if (room != null)
                {
                    RefreshPlayersList(room.ID);
                }
            }
        
                 
        }


        private void JoinRoom(Room room)
        {
            RoomWindow window = new RoomWindow(room, false);
            this.Hide();
            window.ShowDialog();
            this.Close();
        }

    

        private bool TryJoinRoom(Room room)
        {
            string message = Protocol.USR_ROOM_JOIN.ToString() + Utils.ParseIntToXBytes(room.ID, 4);

            //Send message

            /*
            byte[] buffer = new ASCIIEncoding().GetBytes(message);
            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
            */

            Utils.SendEncrypted(message);

            Utils.ReadEncrypted();

            //Recieve messageCode
            /*buffer = new byte[3];
            int bytesRead = clientStream.Read(buffer, 0, 3);
            */
            Utils.ReadFromStream(3);

            //Recieve returnCode
            /*buffer = new byte[1];
            bytesRead = clientStream.Read(buffer, 0, 1);
            string input = new ASCIIEncoding().GetString(buffer);
             * */
            int code = int.Parse(Utils.ReadFromStream(1));

            if(code == 0)
            {
                //Reading questionsNumber

                /*
                buffer = new byte[2];
                bytesRead = clientStream.Read(buffer, 0, 2);
                input = new ASCIIEncoding().GetString(buffer);
                 * */
                room.QuestionsNum = int.Parse(Utils.ReadFromStream(2));

                //Reading questionsTime
                /*
                buffer = new byte[2];
                bytesRead = clientStream.Read(buffer, 0, 2);
                input = new ASCIIEncoding().GetString(buffer);
                 * */
                room.QuestionTime = int.Parse(Utils.ReadFromStream(2));
                return true;
            }

            lbl_error.Visible = true;
            lbl_error.Text = "Error: ";
           switch(code)
            {
                case 1:
                    lbl_error.Text += "Room is full.";
                    break;
                case 2:
                    lbl_error.Text += "Room might not exist.";
                    break;
            }


            return false;
        }

        private void JoinSelectedRoom(object sender, EventArgs e)
        {
            Room room = (Room)lstb_rooms.Items[lstb_rooms.SelectedIndex];

            if(room == null)
            {
                return;
            }

            if(TryJoinRoom(room))
            {
                JoinRoom(room);
            }
        }

        private void BackToMenu(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
