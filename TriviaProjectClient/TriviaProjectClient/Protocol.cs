﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaProjectClient
{
    static class Protocol
    {
        
        public const int USR_SIGNIN = 200;
        public const int USR_SIGNOUT = 201;
        public const int USR_SIGNUP = 203;
        public const int USR_ROOM_REQUEST = 205;
        public const int USR_ROOM_PLAYERLIST = 207;
        public const int USR_ROOM_LEAVE = 211; 
        public const int USR_ROOM_JOIN = 209;
        public const int USR_ROOM_CLOSE = 215;
        public const int USR_ROOM_CREATE = 213;
        public const int USR_LOGOUT = 299;
        public const int USR_GAME_ANSWER = 219;
        public const int USR_GAME_LEAVE = 222;
        public const int USR_GAME_START = 217;
        public const int USR_HIGHSCORE = 223;
        public const int USR_PERSONAL_STATUS = 225;

        public const int SRV_SIGNUP = 104;
        public const int SRV_SIGNIN = 102;
        public const int SRV_SIGNIN_SUCCESS = 1020;
        public const int SRV_SIGNIN_WRONG = 1021;
        public const int SRV_SIGNIN_LOGGED = 1022;
        public const int SRV_ROOM_LIST = 106;
        public const int SRV_ROOM_PLAYERLIST = 108;
        public const int SRV_ROOM_JOIN_RESPONSE = 110;
        public const int SRV_ROOM_CLOSING = 116;
        public const int SRV_ROOM_CREATION_RESPONSE = 114;
        public const int SRV_GAME_QUESTION = 118;
        public const int SRV_GAME_ANSWER = 120;
        public const int SRV_GAME_FINISH = 121;
        public const int SRV_HIGHSCORE_RESPONSE = 124;
        public const int SRV_PERSONAL_STATUS_RESPONSE = 126;
        public const int USR_CHATROOM_MSG = 300;
        public const int SRV_CHATROOM_MSG = 301;

        public const int SRV_KEY = 401;
        public const int USR_KEY = 400;
           
    }
}
