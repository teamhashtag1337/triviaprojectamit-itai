﻿namespace TriviaProjectClient
{
    partial class ConfigWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_ip = new System.Windows.Forms.Label();
            this.lbl_port = new System.Windows.Forms.Label();
            this.txtb_ip = new System.Windows.Forms.TextBox();
            this.txtb_port = new System.Windows.Forms.TextBox();
            this.btn_startgame = new System.Windows.Forms.Button();
            this.lbl_error = new System.Windows.Forms.Label();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_quit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_ip
            // 
            this.lbl_ip.AutoSize = true;
            this.lbl_ip.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ip.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_ip.Location = new System.Drawing.Point(34, 67);
            this.lbl_ip.Name = "lbl_ip";
            this.lbl_ip.Size = new System.Drawing.Size(68, 46);
            this.lbl_ip.TabIndex = 0;
            this.lbl_ip.Text = "IP:";
            // 
            // lbl_port
            // 
            this.lbl_port.AutoSize = true;
            this.lbl_port.BackColor = System.Drawing.Color.Transparent;
            this.lbl_port.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_port.Location = new System.Drawing.Point(34, 143);
            this.lbl_port.Name = "lbl_port";
            this.lbl_port.Size = new System.Drawing.Size(105, 46);
            this.lbl_port.TabIndex = 1;
            this.lbl_port.Text = "Port:";
            // 
            // txtb_ip
            // 
            this.txtb_ip.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtb_ip.Location = new System.Drawing.Point(154, 75);
            this.txtb_ip.Name = "txtb_ip";
            this.txtb_ip.Size = new System.Drawing.Size(300, 38);
            this.txtb_ip.TabIndex = 2;
            // 
            // txtb_port
            // 
            this.txtb_port.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtb_port.Location = new System.Drawing.Point(154, 151);
            this.txtb_port.Name = "txtb_port";
            this.txtb_port.Size = new System.Drawing.Size(300, 38);
            this.txtb_port.TabIndex = 3;
            // 
            // btn_startgame
            // 
            this.btn_startgame.Location = new System.Drawing.Point(72, 232);
            this.btn_startgame.Name = "btn_startgame";
            this.btn_startgame.Size = new System.Drawing.Size(146, 59);
            this.btn_startgame.TabIndex = 4;
            this.btn_startgame.Text = "Start Game";
            this.btn_startgame.UseVisualStyleBackColor = true;
            this.btn_startgame.Click += new System.EventHandler(this.StartGame);
            // 
            // lbl_error
            // 
            this.lbl_error.AutoSize = true;
            this.lbl_error.BackColor = System.Drawing.Color.Transparent;
            this.lbl_error.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_error.ForeColor = System.Drawing.Color.Red;
            this.lbl_error.Location = new System.Drawing.Point(202, 39);
            this.lbl_error.Name = "lbl_error";
            this.lbl_error.Size = new System.Drawing.Size(82, 25);
            this.lbl_error.TabIndex = 5;
            this.lbl_error.Text = "lbl_error";
            this.lbl_error.Visible = false;
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(372, 195);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(82, 24);
            this.btn_save.TabIndex = 6;
            this.btn_save.Text = "Save Config";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.SaveConfig);
            // 
            // btn_quit
            // 
            this.btn_quit.Location = new System.Drawing.Point(246, 232);
            this.btn_quit.Name = "btn_quit";
            this.btn_quit.Size = new System.Drawing.Size(146, 59);
            this.btn_quit.TabIndex = 7;
            this.btn_quit.Text = "Quit";
            this.btn_quit.UseVisualStyleBackColor = true;
            this.btn_quit.Click += new System.EventHandler(this.ExitGame);
            // 
            // ConfigWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TriviaProjectClient.Properties.Resources.config_img;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(484, 303);
            this.Controls.Add(this.btn_quit);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.lbl_error);
            this.Controls.Add(this.btn_startgame);
            this.Controls.Add(this.txtb_port);
            this.Controls.Add(this.txtb_ip);
            this.Controls.Add(this.lbl_port);
            this.Controls.Add(this.lbl_ip);
            this.DoubleBuffered = true;
            this.Name = "ConfigWindow";
            this.Text = "ConfigWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        private System.Windows.Forms.Label lbl_ip;
        private System.Windows.Forms.Label lbl_port;
        private System.Windows.Forms.TextBox txtb_ip;
        private System.Windows.Forms.TextBox txtb_port;
        private System.Windows.Forms.Button btn_startgame;
        private System.Windows.Forms.Label lbl_error;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_quit;
    }
}