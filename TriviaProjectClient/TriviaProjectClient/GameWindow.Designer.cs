﻿namespace TriviaProjectClient
{
    partial class GameWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_question = new System.Windows.Forms.Label();
            this.btn_ans1 = new System.Windows.Forms.Button();
            this.btn_ans2 = new System.Windows.Forms.Button();
            this.btn_ans3 = new System.Windows.Forms.Button();
            this.btn_ans4 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_time = new System.Windows.Forms.Label();
            this.lbl_timeleft = new System.Windows.Forms.Label();
            this.lbl_scoretext = new System.Windows.Forms.Label();
            this.lbl_score = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_question
            // 
            this.lbl_question.AutoSize = true;
            this.lbl_question.BackColor = System.Drawing.Color.Transparent;
            this.lbl_question.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_question.Location = new System.Drawing.Point(12, 79);
            this.lbl_question.Name = "lbl_question";
            this.lbl_question.Size = new System.Drawing.Size(233, 46);
            this.lbl_question.TabIndex = 0;
            this.lbl_question.Text = "lbl_question";
            // 
            // btn_ans1
            // 
            this.btn_ans1.BackColor = System.Drawing.Color.Transparent;
            this.btn_ans1.Location = new System.Drawing.Point(248, 242);
            this.btn_ans1.Name = "btn_ans1";
            this.btn_ans1.Size = new System.Drawing.Size(269, 68);
            this.btn_ans1.TabIndex = 1;
            this.btn_ans1.Text = "ans1";
            this.btn_ans1.UseVisualStyleBackColor = false;
            this.btn_ans1.Click += new System.EventHandler(this.SendAnswer);
            // 
            // btn_ans2
            // 
            this.btn_ans2.BackColor = System.Drawing.Color.Transparent;
            this.btn_ans2.Location = new System.Drawing.Point(248, 316);
            this.btn_ans2.Name = "btn_ans2";
            this.btn_ans2.Size = new System.Drawing.Size(269, 68);
            this.btn_ans2.TabIndex = 2;
            this.btn_ans2.Text = "ans2";
            this.btn_ans2.UseVisualStyleBackColor = false;
            this.btn_ans2.Click += new System.EventHandler(this.SendAnswer);
            // 
            // btn_ans3
            // 
            this.btn_ans3.BackColor = System.Drawing.Color.Transparent;
            this.btn_ans3.Location = new System.Drawing.Point(248, 390);
            this.btn_ans3.Name = "btn_ans3";
            this.btn_ans3.Size = new System.Drawing.Size(269, 68);
            this.btn_ans3.TabIndex = 3;
            this.btn_ans3.Text = "ans3";
            this.btn_ans3.UseVisualStyleBackColor = false;
            this.btn_ans3.Click += new System.EventHandler(this.SendAnswer);
            // 
            // btn_ans4
            // 
            this.btn_ans4.BackColor = System.Drawing.Color.Transparent;
            this.btn_ans4.Location = new System.Drawing.Point(248, 464);
            this.btn_ans4.Name = "btn_ans4";
            this.btn_ans4.Size = new System.Drawing.Size(269, 68);
            this.btn_ans4.TabIndex = 4;
            this.btn_ans4.Text = "ans4";
            this.btn_ans4.UseVisualStyleBackColor = false;
            this.btn_ans4.Click += new System.EventHandler(this.SendAnswer);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.lbl_time);
            this.panel1.ForeColor = System.Drawing.Color.Transparent;
            this.panel1.Location = new System.Drawing.Point(44, 316);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(114, 93);
            this.panel1.TabIndex = 5;
            // 
            // lbl_time
            // 
            this.lbl_time.AutoSize = true;
            this.lbl_time.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_time.ForeColor = System.Drawing.Color.Black;
            this.lbl_time.Location = new System.Drawing.Point(27, 27);
            this.lbl_time.Name = "lbl_time";
            this.lbl_time.Size = new System.Drawing.Size(64, 46);
            this.lbl_time.TabIndex = 0;
            this.lbl_time.Text = "99";
            // 
            // lbl_timeleft
            // 
            this.lbl_timeleft.AutoSize = true;
            this.lbl_timeleft.BackColor = System.Drawing.Color.Transparent;
            this.lbl_timeleft.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_timeleft.Location = new System.Drawing.Point(38, 279);
            this.lbl_timeleft.Name = "lbl_timeleft";
            this.lbl_timeleft.Size = new System.Drawing.Size(126, 31);
            this.lbl_timeleft.TabIndex = 6;
            this.lbl_timeleft.Text = "Time left:";
            // 
            // lbl_scoretext
            // 
            this.lbl_scoretext.AutoSize = true;
            this.lbl_scoretext.BackColor = System.Drawing.Color.Transparent;
            this.lbl_scoretext.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_scoretext.Location = new System.Drawing.Point(38, 427);
            this.lbl_scoretext.Name = "lbl_scoretext";
            this.lbl_scoretext.Size = new System.Drawing.Size(93, 31);
            this.lbl_scoretext.TabIndex = 8;
            this.lbl_scoretext.Text = "Score:";
            // 
            // lbl_score
            // 
            this.lbl_score.AutoSize = true;
            this.lbl_score.BackColor = System.Drawing.Color.Transparent;
            this.lbl_score.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_score.Location = new System.Drawing.Point(129, 427);
            this.lbl_score.Name = "lbl_score";
            this.lbl_score.Size = new System.Drawing.Size(29, 31);
            this.lbl_score.TabIndex = 9;
            this.lbl_score.Text = "0";
            // 
            // GameWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TriviaProjectClient.Properties.Resources.brain_img;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.lbl_score);
            this.Controls.Add(this.lbl_scoretext);
            this.Controls.Add(this.lbl_timeleft);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_ans4);
            this.Controls.Add(this.btn_ans3);
            this.Controls.Add(this.btn_ans2);
            this.Controls.Add(this.btn_ans1);
            this.Controls.Add(this.lbl_question);
            this.Name = "GameWindow";
            this.Text = "GameWindow";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_question;
        private System.Windows.Forms.Button btn_ans1;
        private System.Windows.Forms.Button btn_ans2;
        private System.Windows.Forms.Button btn_ans3;
        private System.Windows.Forms.Button btn_ans4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_time;
        private System.Windows.Forms.Label lbl_timeleft;
        private System.Windows.Forms.Label lbl_scoretext;
        private System.Windows.Forms.Label lbl_score;
    }
}