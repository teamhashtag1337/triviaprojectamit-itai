﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaProjectClient
{
    public partial class RoomCreationWindow : Form
    {
        private NetworkStream clientStream;

        public RoomCreationWindow()
        {
            InitializeComponent();
            this.clientStream = Config.socket;
        }

        private Room TryCreateRoom() // Returns a room if succeeded, null if failed.
        {
            string message = "";
            try
            {
                message = Protocol.USR_ROOM_CREATE.ToString();
                message += Utils.ParseIntToXBytes(txtb_roomname.Text.Length, 2);
                message += txtb_roomname.Text;
                message += Utils.ParseIntToXBytes(int.Parse(txtb_numplayers.Text), 1);
                message += Utils.ParseIntToXBytes(int.Parse(txtb_numquestions.Text), 2);
                message += Utils.ParseIntToXBytes(int.Parse(txtb_questiontime.Text), 2);
            }

            catch(Exception e)
            {
                lbl_error.Text = "Error, illegal input";
                lbl_error.Visible = true;
                return null;
            }
       

            //Send message
            /*
            byte[] buffer = new ASCIIEncoding().GetBytes(message);
            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
            */
            Utils.SendEncrypted(message);
            Utils.ReadEncrypted();

            //Recieve messageCode
            /*
            buffer = new byte[4];
            int bytesRead = clientStream.Read(buffer, 0, 4);
             * */
            string input = Utils.ReadFromStream(4);

            switch (input[3])
            {
                case '0': //Success
                    Room room = new Room(-1, txtb_roomname.Text); //ID is not needed for an admin so it`s set to -1.
                    room.MaxPlayers = int.Parse(txtb_numplayers.Text);
                    room.QuestionsNum = int.Parse(txtb_numquestions.Text);
                    room.QuestionTime = int.Parse(txtb_questiontime.Text);                 
                    return room;
                    
                case '1': //Failure
                    lbl_error.Visible = true;
                    lbl_error.Text = "Room creation failed.";
                    return null;
                default:
                    return null;
                    
            }

        }

        private void CreateRoom(Room room)
        {
            RoomWindow window = new RoomWindow(room, true);
            this.Hide();
            window.ShowDialog();
            this.Close();
        }

        private void BackToMenu(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CreateRoom(object sender, EventArgs e)
        {
            Room room = TryCreateRoom();

            if(room != null)
            {
                CreateRoom(room);
            }
        }
    }
}
