﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaProjectClient
{
    public partial class GameWindow : Form
    {
        private int timeLeft;
        private int timePerQuestion;
        private int score = 0;
        private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        private Button selectedAnswer = null;
        NetworkStream clientStream = Config.socket;
        private bool killListener = false;


        public GameWindow(int timePerQuestion)
        {
            this.FormClosing += HandleClosing;
            this.timePerQuestion = timePerQuestion;
            timer.Tick += AdvanceTick;
            InitializeComponent();
            FetchQuestion();

            if (!this.IsHandleCreated)
            {
                this.CreateHandle();
            }

            Thread listener = new Thread(Listen);
            listener.Start();
            
        }

        private void HandleClosing(object sender, FormClosingEventArgs e)
        {
            killListener = true;
        }


        private void Listen()
        {
            while(!killListener)
            {
                Utils.ReadEncrypted();
                /*
                byte[] buffer = new byte[3];
                int bytesRead = clientStream.Read(buffer, 0, 3);
                string input = new ASCIIEncoding().GetString(buffer);
                 * */
                int msgCode = int.Parse(Utils.ReadFromStream(3));

                switch(msgCode)
                {
                    case Protocol.SRV_GAME_QUESTION:
                        Invoke((MethodInvoker)delegate {
                            FetchQuestion();
                        });                      
                        break;
                    case Protocol.SRV_GAME_ANSWER:
                        FetchAnswer();
                        break;
                    case Protocol.SRV_GAME_FINISH:
                        FinishGame();
                        break;
                }
            }
        }

        private void FetchQuestion()
        {

            //Reading question and length
            /*
            byte[] buffer = new byte[3];
            int bytesRead = clientStream.Read(buffer, 0, 3);
            string input = new ASCIIEncoding().GetString(buffer);
             * */
            int questionLength = int.Parse(Utils.ReadFromStream(3));

            if(questionLength == 0)
            {
                MessageBox.Show("Error fetching question.");
                this.Close();
                return;
            }

            /*
            buffer = new byte[questionLength];
            bytesRead = clientStream.Read(buffer, 0, questionLength);
            input = new ASCIIEncoding().GetString(buffer);
             * */
            string question = Utils.ReadFromStream(questionLength);

            string[] answers = new string[4];

            for(int i = 0; i < answers.Length; i++)
            {
                //Reading answer and length
                /*
                buffer = new byte[3];
                bytesRead = clientStream.Read(buffer, 0, 3);
                input = new ASCIIEncoding().GetString(buffer);
                 * */
                int answerLength = int.Parse(Utils.ReadFromStream(3));

                /*
                buffer = new byte[answerLength];
                bytesRead = clientStream.Read(buffer, 0, answerLength);
                 * */
                string input = Utils.ReadFromStream(answerLength);
                answers[i] = input;
            }

            InitQuestion(question, answers[0], answers[1], answers[2], answers[3]);

        }

        private void InitQuestion(string question, string ans1, string ans2, string ans3, string ans4)
        {
            if (selectedAnswer != null)
            {
                Invoke((MethodInvoker)delegate {
                    selectedAnswer.BackColor = SystemColors.Control;
                });
               
            }
            timeLeft = timePerQuestion;
            selectedAnswer = null;
            lbl_question.Text = question;
            btn_ans1.Text = ans1;
            btn_ans2.Text = ans2;
            btn_ans3.Text = ans3;
            btn_ans4.Text = ans4;
            lbl_time.Text = timeLeft.ToString();
            timeLeft = timePerQuestion;        
            timer.Interval = (int)TimeSpan.FromSeconds(1.0).TotalMilliseconds;
            timer.Start();
            btn_ans1.Enabled = true;
            btn_ans2.Enabled = true;
            btn_ans3.Enabled = true;
            btn_ans4.Enabled = true;
        }

        private void AdvanceTick(object sender, EventArgs e)
        {
            timeLeft--;
            if(timeLeft < 0)
            {            
                timer.Stop();
                SendBlankAnswer();
                return;
            }
            lbl_time.Text = timeLeft.ToString();
        }

        private void SendBlankAnswer()
        {
            SendAnswer(0);
        }

        private void SendAnswer(int answer)
        {
            timer.Stop();
            btn_ans1.Enabled = false;
            btn_ans2.Enabled = false;
            btn_ans3.Enabled = false;
            btn_ans4.Enabled = false;
            string message = Protocol.USR_GAME_ANSWER.ToString();

            message += Utils.ParseIntToXBytes(answer, 1);
            message += Utils.ParseIntToXBytes(timePerQuestion - timeLeft, 2);

            //Send message

            /*
            byte[] buffer = new ASCIIEncoding().GetBytes(message);
            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
            */

            Utils.SendEncrypted(message);
        }

        private void SendAnswer(object sender, EventArgs e)
        {
            selectedAnswer = (Button)sender;
            SendAnswer(int.Parse(selectedAnswer.Name[selectedAnswer.Name.Length - 1].ToString()));
        }

        private void FinishGame()
        {
            string results = "";

            /*
            byte[] buffer = new byte[1];
            int bytesRead = clientStream.Read(buffer, 0, 1);
            string input = new ASCIIEncoding().GetString(buffer);
             * */
            int numOfUsers = int.Parse(Utils.ReadFromStream(1));

            for(int i = 0; i < numOfUsers; i++)
            {
                //Reading name length
                /*
                buffer = new byte[2];
                bytesRead = clientStream.Read(buffer, 0, 2);
                input = new ASCIIEncoding().GetString(buffer);
                 * */
                int usernameLength = int.Parse(Utils.ReadFromStream(2));

                //Reading name
                /*
                buffer = new byte[usernameLength];
                bytesRead = clientStream.Read(buffer, 0, usernameLength);
                 * */
                string input = Utils.ReadFromStream(usernameLength);

                results += input + ": ";

                //Reading score

                /*
                buffer = new byte[2];
                bytesRead = clientStream.Read(buffer, 0, 2);
                 * */
                input = Utils.ReadFromStream(2);
                results += int.Parse(input).ToString() + "\n";
              
            }

            string message = Protocol.USR_GAME_LEAVE.ToString();


            //Send leave message

            /*
            buffer = new ASCIIEncoding().GetBytes(message);
            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
            */

            Utils.SendEncrypted(message);

            Invoke((MethodInvoker)delegate {
                MessageBox.Show(results);
                this.Close();
            });
                   
        }
        private void FetchAnswer()
        {
            /*
            byte[] buffer = new byte[1];
            int bytesRead = clientStream.Read(buffer, 0, 1);
            string input = new ASCIIEncoding().GetString(buffer);
             * */
            int isCorrect = int.Parse(Utils.ReadFromStream(1));

           
            if (isCorrect == 1)
            {
                if(selectedAnswer != null)
                {                   
                    score++;
                    Invoke((MethodInvoker)delegate { selectedAnswer.BackColor = Color.Green;
                        lbl_score.Text = score.ToString(); }); 
                }
            }

            else
            {
                if (selectedAnswer != null)
                {
                    Invoke((MethodInvoker)delegate {
                        selectedAnswer.BackColor = Color.Red;
                    });
                }
            }

            Thread.Sleep(2000);
        }
    }
}
