﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TriviaProjectClient
{
    class Utils
    {

        public static string ParseIntToXBytes(int number, int bytes)
        {
            int currentNumber = number % (int)Math.Pow(10, bytes);
            string rtn = "";

            while(currentNumber != 0)
            {
                rtn += (currentNumber % 10).ToString();
                currentNumber /= 10;
            }

            while(rtn.Length < bytes)
            {
                rtn += "0";
            }

            char[] charArr = rtn.ToCharArray();
            Array.Reverse(charArr);
            rtn = new string(charArr);

            return rtn;
        }

        public static Stream GenerateStream(string input)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);

            writer.Write(input);
            writer.Flush();
            stream.Position = 0;


            return stream;
        }

        public static string ReadFromStream(int count)
        {
            byte[] buffer = new byte[count];
            Config.stringStream.Read(buffer, 0, count);
            string rtn = "";

            foreach(var ch in buffer)
            {
                rtn += (char)ch;
            }

            return rtn;
        }

        public static void ReadEncrypted()
        {
            NetworkStream clientStream = Config.socket;
            byte[] buffer = new byte[Cryptor.KEY_LENGTH_BYTES];
            int bytesRead = clientStream.Read(buffer, 0, Cryptor.KEY_LENGTH_BYTES);
            if(Config.stringStream != null)
            {
                Config.stringStream.Dispose();
            }

            string decrypted = Config.cryptor.Decrypt(buffer);

            Config.stringStream = GenerateStream(decrypted);
        }

        public static void SendEncrypted(string message)
        {
            NetworkStream clientStream = Config.socket;
            byte[] buffer = Config.cryptor.Encrypt(message);
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
        }
    }
}
