﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TriviaProjectClient
{
    class Config
    {
        public static string ip = "192.168.14.97";
        public static int port = 8820;
        public static NetworkStream socket = null;
        public static Stream stringStream;
        public static string username = "";
        public static Cryptor cryptor;
    }
}
