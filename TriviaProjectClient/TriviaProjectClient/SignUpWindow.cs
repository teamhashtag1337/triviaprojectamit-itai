﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaProjectClient
{
    public partial class SignUpWindow : Form
    {
        private NetworkStream clientStream;

        public SignUpWindow()
        {
            InitializeComponent();
            this.clientStream = Config.socket;
        }

        private bool TrySignUp(string username, string password, string email)
        {
            string message = Protocol.USR_SIGNUP.ToString(); // Sign Up
            message += Utils.ParseIntToXBytes(username.Length, 2);
            message += username;
            message += Utils.ParseIntToXBytes(password.Length, 2);
            message += password;
            message += Utils.ParseIntToXBytes(email.Length, 2);
            message += email;

            //Send message

            /*
            byte[] buffer = new ASCIIEncoding().GetBytes(message);
            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
            */

            Utils.SendEncrypted(message);
            Utils.ReadEncrypted();

            //Recieve answer
            /*
            buffer = new byte[4];
            int bytesRead = clientStream.Read(buffer, 0, 4);*/
            string input = Utils.ReadFromStream(4);

            if(input == "1040") //Success
            {               
                return true;
            }

            lbl_error.Visible = true;
            lbl_error.ForeColor = Color.Red;
            lbl_error.Text = "Error: ";

            switch(input)
            {
                case "1041": // Pass Illegal
                    lbl_error.Text += "Password is illegal.";
                    break;
     
                case "1042": //Username already exists
                    lbl_error.Text += "Username already exists.";
                    break;

                case "1043": //Username illegal
                    lbl_error.Text += "Username is illegal.";
                    break;

                case "1044": //Other
                    lbl_error.Text += "Unknown error. Might be a database error.";
                    break;
            }

            return false;


        }

        private void SignUp(object sender, EventArgs e)
        {
            if(TrySignUp(txtb_uname.Text, txtb_pass.Text, txtb_email.Text))
            {
                lbl_error.Visible = true;
                lbl_error.ForeColor = Color.White;
                lbl_error.Text = "Successfully registered!";
            }
        }

        private void BackToMenu(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
