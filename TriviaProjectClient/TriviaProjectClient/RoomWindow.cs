﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaProjectClient
{
    public partial class RoomWindow : Form
    {
        NetworkStream clientStream;
        Room currentRoom;
        Thread listener;
        bool isClosed = false;
        bool killListener = false;
        bool isAdmin = false;
        bool doneFetchPlayers = false;
        ChatRoomWindow chatRoom;

        public RoomWindow(Room room, bool isAdmin) 
        {
            this.FormClosing += QuitProperly;
            InitializeComponent();
            lbl_numquestions.Text = "Number of questions: " + room.QuestionsNum;
            lbl_timeperquestion.Text = "Time per question: " + room.QuestionTime;
            lbl_title.Text = room.Name;
            this.isAdmin = isAdmin;

            this.clientStream = Config.socket;
            this.currentRoom = room;

            if (isAdmin)
            {
                btn_leave.Location = new Point(176, 460);
                Button startGame = new Button();
                startGame.Width = btn_leave.Width;
                startGame.Height = btn_leave.Height;
                startGame.Location = new Point(433, 460);
                startGame.BackColor = btn_leave.BackColor;
                startGame.Text = "Start Game";
                startGame.Click += SendStartGame;
                this.Controls.Add(startGame);
                lbl_maxplayers.Visible = true;
                lbl_maxplayers.Text = "Max players: " + room.MaxPlayers;
                lstb_players.Items.Add(Config.username);
            }

            if(!this.IsHandleCreated)
            {
                this.CreateHandle();
            }

            Thread thread = new Thread(ShowChatWindow);
            thread.Start();          
           
            listener = new Thread(Listen);
            listener.Start();
        }

        private void QuitProperly(object sender, FormClosingEventArgs e)
        {
            Invoke((MethodInvoker)delegate { chatRoom.Close(); });
        }

        private void ShowChatWindow()
        {
            chatRoom = new ChatRoomWindow();
            chatRoom.ShowDialog();
 
        }
        private void SendStartGame(object sender, EventArgs e)
        {
            string message = Protocol.USR_GAME_START.ToString();


            /*
            //Send message
            byte[] buffer = new ASCIIEncoding().GetBytes(message);
            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
             * */

            Utils.SendEncrypted(message);
        }

        private void Listen()
        {
            while(!killListener)
            {
                Utils.ReadEncrypted();

                //Recieve message
                /*
                byte[] buffer = new byte[3];
                int bytesRead = clientStream.Read(buffer, 0, 3);
                 * 
                string input = new ASCIIEncoding().GetString(buffer);
                 * */
                int msgCode = int.Parse(Utils.ReadFromStream(3));

                switch(msgCode)
                {
                    case Protocol.SRV_ROOM_PLAYERLIST:              
                        Invoke((MethodInvoker)delegate { FetchPlayers(); });
                        while(!doneFetchPlayers)
                        {

                        }

                        doneFetchPlayers = false;
                        break;
                    case Protocol.SRV_ROOM_CLOSING:
                        CloseRoom();
                        return;
                    case Protocol.SRV_GAME_QUESTION:
                        StartGame(currentRoom.QuestionTime);
                        break;
                    case Protocol.SRV_CHATROOM_MSG:
                        Invoke((MethodInvoker)delegate { chatRoom.DigestMessage(); });      
                        break;
                    
                    case Protocol.SRV_ROOM_CREATION_RESPONSE:
                    //case Protocol.SRV_ROOM_JOIN_RESPONSE:
                        Utils.ReadFromStream(1);
                        break;
                }

       
            }
 
        }

        private void CloseRoom()
        {
            if(!isAdmin)
            {
                Invoke((MethodInvoker)delegate
                {
                    lbl_error.ForeColor = Color.White;
                    lbl_error.Font = new Font("Microsoft Sans Serif", 20);
                    lbl_error.Visible = true;
                    lbl_error.Text = "The room has been closed. Please return to menu.";
                });

            }
      
            this.isClosed = true;
           
        }

        private void FetchPlayers()
        {
            lstb_players.Items.Clear();

            /*
            byte[] buffer = new byte[1];

            //Reading number of players
            int bytesRead = clientStream.Read(buffer, 0, 1);
            string input = new ASCIIEncoding().GetString(buffer);
             * */
            int numPlayers = int.Parse(Utils.ReadFromStream(1));


            for (int i = 0; i < numPlayers; i++)
            {
                //Reading username length
                /*
                buffer = new byte[2];
                bytesRead = clientStream.Read(buffer, 0, 2);
                input = new ASCIIEncoding().GetString(buffer);
                 * */
                int nameLength = int.Parse(Utils.ReadFromStream(2));

                //Reading username
                /*
                buffer = new byte[nameLength];
                bytesRead = clientStream.Read(buffer, 0, nameLength);
                 * */
                string input = Utils.ReadFromStream(nameLength);
                lstb_players.Items.Add(input);
            }

            doneFetchPlayers = true;
        }


        private void StartGame(int timePerQuestion)
        {
            Invoke((MethodInvoker)delegate
            {
                killListener = true;
                try
                {
                    
                    GameWindow window = new GameWindow(timePerQuestion);
                  //  chatRoom.Close();
                    this.Hide();
                    if (window != null)
                    {
                        window.ShowDialog();
                    }
                    this.Close();
                }

                catch(Exception e)
                {
                    MessageBox.Show("Error starting the game.");
                }
     
            });
        
        }


        private void LeaveRoom(object sender, EventArgs e)
        {
            killListener = true;
            string message = "";


            if (!isAdmin)
            {
                message = Protocol.USR_ROOM_LEAVE.ToString();
                //Send message
                /*
                byte[] buffer = new ASCIIEncoding().GetBytes(message);
                clientStream.Write(buffer, 0, message.Length);
                clientStream.Flush();
                */

                Utils.SendEncrypted(message);
            }

            else
            {
                message = Protocol.USR_ROOM_CLOSE.ToString();
                //Send message
                /*
                byte[] buffer = new ASCIIEncoding().GetBytes(message);
                clientStream.Write(buffer, 0, message.Length);
                clientStream.Flush();
            */
                Utils.SendEncrypted(message);
              
            }
      
            this.Close();
        }
    }
}
