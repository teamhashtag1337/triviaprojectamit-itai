﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace TriviaProjectClient
{
    public partial class ConfigWindow : Form
    {
        private const string PATH = @"config.xml";
        private NetworkStream clientStream;

        public ConfigWindow()
        {
            InitializeComponent();
            InitXml();
            txtb_ip.Text = Config.ip;
            txtb_port.Text = Config.port.ToString();
        }

        private void HandleClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            WriteToXml();
        }


        private void InitXml()
        {
            try
            {
                ReadFromXml();
            }
            catch(Exception e)
            {
                WriteToXml();
            }

        }
        private void StartGame(object sender, EventArgs e)
        {
            try
            {
                if(txtb_ip.Text.Length == 0)
                {
                    throw new Exception("Invalid input");
                }

                Config.ip = txtb_ip.Text;
                Config.port = int.Parse(txtb_port.Text);
                WriteToXml();
                ConnectToServer();
                clientStream = Config.socket;
                Cryptor cryptor = new Cryptor();


                RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
                RSAParameters RSAKeyInfo = new RSAParameters();

                byte[] buffer = new byte[3];
                int bytesRead = clientStream.Read(buffer, 0, 3);
                string input = new ASCIIEncoding().GetString(buffer);
                int code = int.Parse(input);
                if (code != Protocol.SRV_KEY) //Handle unexpected message (should be key)
                {
                    throw new Exception("Failed to recieve the server's key.");
                }

                buffer = new byte[4];
                bytesRead = clientStream.Read(buffer, 0, 4);
                input = new ASCIIEncoding().GetString(buffer);
                int len = int.Parse(input);

                buffer = new byte[len];
                bytesRead = clientStream.Read(buffer, 0, len);
                RSAKeyInfo.Exponent = buffer;
                
                buffer = new byte[Cryptor.KEY_LENGTH / 8];
                bytesRead = clientStream.Read(buffer, 0, Cryptor.KEY_LENGTH / 8);
                RSAKeyInfo.Modulus = buffer;

                RSA.ImportParameters(RSAKeyInfo);

                cryptor.friendPublicKey = RSAKeyInfo;
                /*
                for(int i = 0; i < buffer.Length; i++)
                {
                    cryptor.friendPublicKey.Modulus[i] = buffer[i];
                }*/

                /*string message = Protocol.SRV
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();*/

                string message = Protocol.USR_KEY.ToString();
                buffer = new ASCIIEncoding().GetBytes(message);
                clientStream.Write(buffer, 0, message.Length);
 

                message = Utils.ParseIntToXBytes(cryptor.publicKey.Exponent.Length, 4);
                buffer = new ASCIIEncoding().GetBytes(message);
                clientStream.Write(buffer, 0, message.Length);


                buffer = cryptor.publicKey.Exponent;
                clientStream.Write(buffer, 0, buffer.Length);


                buffer = cryptor.publicKey.Modulus;
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
                Config.cryptor = cryptor;
                MainMenuWindow window = new MainMenuWindow();
                this.Hide();
                window.ShowDialog();
                this.Close();
            }

            catch(Exception ex)
            {
                lbl_error.ForeColor = Color.Red;
                lbl_error.Text = "Invalid input. Error: " + ex.Message;
                lbl_error.Visible = true;
            }
        }

        private static void ConnectToServer()
        {
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(Config.ip), Config.port);
            client.Connect(serverEndPoint);
            NetworkStream clientStream = client.GetStream();
            Config.socket = clientStream;
        }

        private void ReadFromXml()
        {
            XmlReader reader = XmlReader.Create(PATH);

            while(reader.Read())
            {
                if(reader.NodeType == XmlNodeType.Element)
                {
                    string elementName = reader.Name;

                    if(elementName == "IP")
                    {
                        reader.Read();
                        Config.ip = reader.Value;
                    }

                    else if(elementName == "Port")
                    {
                        reader.Read();
                        Config.port = int.Parse(reader.Value);
                    }
                }
            }

            reader.Close();
        }

        private void WriteToXml()
        {
            XmlWriter writer = XmlWriter.Create(PATH);
            writer.WriteStartDocument();
            writer.WriteStartElement("Config");
            writer.WriteElementString("IP", Config.ip);
            writer.WriteElementString("Port", Config.port.ToString());
            writer.WriteEndElement();
            writer.WriteEndDocument();
            writer.Close();
        }

        private void ExitGame(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SaveConfig(object sender, EventArgs e)
        {
            WriteToXml();
            lbl_error.ForeColor = Color.Black;
            lbl_error.Text = "Config saved.";
            lbl_error.Visible = true;
        }
    }
}
