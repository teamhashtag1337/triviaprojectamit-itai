﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaProjectClient
{
    public enum Mode { LOGIN, LOGOUT };
    
    public partial class MainMenuWindow : Form
    {
        private Mode mode = Mode.LOGIN;
        private string userName = "Wasim";
        private NetworkStream clientStream;

        public MainMenuWindow()
        {
            InitializeComponent();
            this.clientStream = Config.socket;
        }



        private void ToggleButtons(bool mode) // True -> connected, false -> disconnected
        {
            btn_createroom.Enabled = mode;
            btn_joinroom.Enabled = mode;
            btn_mystatus.Enabled = mode;
            btn_bestscores.Enabled = mode;
            txtb_uname.Enabled = !mode;
            txtb_pass.Enabled = !mode;
        }

        private void ToggleLoginGui(bool mode) // True -> connected, false->disconnected
        {
            lbl_hello.Visible = mode;
            pnl_login.BackColor = (mode) ? this.BackColor : Color.LightSteelBlue;
            txtb_pass.Visible = !mode;
            txtb_uname.Visible = !mode;
            btn_signin.Visible = !mode;
            lbl_uname.Visible = !mode;
            lbl_pass.Visible = !mode;
        }

        private bool TryLogIn(string username, string password)
        {
            string message = Protocol.USR_SIGNIN.ToString();
            message += Utils.ParseIntToXBytes(username.Length, 2);
            message += username;
            message += Utils.ParseIntToXBytes(password.Length, 2);
            message += password;

            //Sending message

            Utils.SendEncrypted(message);
            /*
            byte[] buffer = new ASCIIEncoding().GetBytes(message);
            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
            */

            //Recieving decrypted message
            Utils.ReadEncrypted();

            //Reading return code
            /*
            buffer = new byte[4];
            int bytesRead = clientStream.Read(buffer, 0, 4);
            string input = new ASCIIEncoding().GetString(buffer);
            */

            int input = int.Parse(Utils.ReadFromStream(4));

            if(input == Protocol.SRV_SIGNIN_SUCCESS)
            {
                return true;
            }

            if(input == Protocol.SRV_SIGNIN_WRONG)
            {
                lbl_error.Text = "Wrong details";
            }

            if(input == Protocol.SRV_SIGNIN_LOGGED)
            {
                lbl_error.Text = "User is already connected!";
            }

            lbl_error.Visible = true;
            return false;
        }

        private void SendLogOut()
        {
            string message = Protocol.USR_SIGNOUT.ToString(); //Log out

            Utils.SendEncrypted(message);
        }

        private void SignInOut(object sender, EventArgs e)
        {
            if(this.mode == Mode.LOGIN)
            {
                if(TryLogIn(txtb_uname.Text, txtb_pass.Text))
                {
                    userName = txtb_uname.Text;
                    lbl_error.Text = "No error";
                    lbl_error.Visible = false;
                    ToggleButtons(true);
                    btn_signup.Text = "Sign out";
                    btn_signup.Click += SignInOut;
                    btn_signup.Click -= SignUp;
                    this.mode = Mode.LOGOUT;
                    lbl_hello.Text = "Hello, " + userName;
                    ToggleLoginGui(true);
                    Config.username = txtb_uname.Text;
                    this.userName = txtb_uname.Text;
                }

            }

            else
            {
                SendLogOut();
                ToggleButtons(false);
                btn_signup.Text = "Sign up";
                btn_signup.Click -= SignInOut;
                btn_signup.Click += SignUp;
                this.mode = Mode.LOGIN;
                ToggleLoginGui(false);
                Config.username = "";
            }
        
        }

        private void Exit(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SignUp(object sender, EventArgs e)
        {
            SignUpWindow window = new SignUpWindow();
            this.Hide();
            window.ShowDialog();
            this.Show();
        }

        private void JoinRoom(object sender, EventArgs e)
        {
            RoomMenuWindow window = new RoomMenuWindow();
            this.Hide();
            window.ShowDialog();
            this.Show();
        }

        private void CreateRoom(object sender, EventArgs e)
        {
            RoomCreationWindow window = new RoomCreationWindow();
            this.Hide();
            window.ShowDialog();
            this.Show();
        }

        private void BestScores(object sender, EventArgs e)
        {
            BestScoresWindow window = new BestScoresWindow();
            this.Hide();
            window.ShowDialog();
            this.Show();
        }

        private void MyStatus(object sender, EventArgs e)
        {
            PersonalStatusWindow window = new PersonalStatusWindow();
            this.Hide();
            window.ShowDialog();
            this.Show();
        }
    }
}
