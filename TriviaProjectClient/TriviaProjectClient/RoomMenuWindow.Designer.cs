﻿namespace TriviaProjectClient
{
    partial class RoomMenuWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_rooms = new System.Windows.Forms.Label();
            this.lstb_rooms = new System.Windows.Forms.ListBox();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.btn_join = new System.Windows.Forms.Button();
            this.lstb_players = new System.Windows.Forms.ListBox();
            this.lbl_numplayers = new System.Windows.Forms.Label();
            this.lbl_error = new System.Windows.Forms.Label();
            this.btn_back = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_rooms
            // 
            this.lbl_rooms.AutoSize = true;
            this.lbl_rooms.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_rooms.Location = new System.Drawing.Point(146, 35);
            this.lbl_rooms.Name = "lbl_rooms";
            this.lbl_rooms.Size = new System.Drawing.Size(503, 76);
            this.lbl_rooms.TabIndex = 0;
            this.lbl_rooms.Text = "Room Selection";
            // 
            // lstb_rooms
            // 
            this.lstb_rooms.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.lstb_rooms.FormattingEnabled = true;
            this.lstb_rooms.ItemHeight = 46;
            this.lstb_rooms.Location = new System.Drawing.Point(159, 150);
            this.lstb_rooms.Name = "lstb_rooms";
            this.lstb_rooms.ScrollAlwaysVisible = true;
            this.lstb_rooms.Size = new System.Drawing.Size(521, 142);
            this.lstb_rooms.TabIndex = 1;
            this.lstb_rooms.SelectedIndexChanged += new System.EventHandler(this.RoomSelected);
            // 
            // btn_refresh
            // 
            this.btn_refresh.Location = new System.Drawing.Point(582, 298);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(98, 52);
            this.btn_refresh.TabIndex = 2;
            this.btn_refresh.Text = "Refresh";
            this.btn_refresh.UseVisualStyleBackColor = true;
            this.btn_refresh.Click += new System.EventHandler(this.RefreshRoomsList);
            // 
            // btn_join
            // 
            this.btn_join.Location = new System.Drawing.Point(277, 462);
            this.btn_join.Name = "btn_join";
            this.btn_join.Size = new System.Drawing.Size(182, 87);
            this.btn_join.TabIndex = 3;
            this.btn_join.Text = "Join";
            this.btn_join.UseVisualStyleBackColor = true;
            this.btn_join.Click += new System.EventHandler(this.JoinSelectedRoom);
            // 
            // lstb_players
            // 
            this.lstb_players.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lstb_players.FormattingEnabled = true;
            this.lstb_players.ItemHeight = 31;
            this.lstb_players.Location = new System.Drawing.Point(231, 339);
            this.lstb_players.Name = "lstb_players";
            this.lstb_players.Size = new System.Drawing.Size(277, 97);
            this.lstb_players.TabIndex = 4;
            // 
            // lbl_numplayers
            // 
            this.lbl_numplayers.AutoSize = true;
            this.lbl_numplayers.Location = new System.Drawing.Point(228, 323);
            this.lbl_numplayers.Name = "lbl_numplayers";
            this.lbl_numplayers.Size = new System.Drawing.Size(76, 13);
            this.lbl_numplayers.TabIndex = 5;
            this.lbl_numplayers.Text = "lbl_numplayers";
            this.lbl_numplayers.Visible = false;
            // 
            // lbl_error
            // 
            this.lbl_error.AutoSize = true;
            this.lbl_error.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_error.ForeColor = System.Drawing.Color.Red;
            this.lbl_error.Location = new System.Drawing.Point(228, 439);
            this.lbl_error.Name = "lbl_error";
            this.lbl_error.Size = new System.Drawing.Size(61, 17);
            this.lbl_error.TabIndex = 6;
            this.lbl_error.Text = "lbl_error";
            this.lbl_error.Visible = false;
            // 
            // btn_back
            // 
            this.btn_back.Location = new System.Drawing.Point(663, 12);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(109, 60);
            this.btn_back.TabIndex = 7;
            this.btn_back.Text = "Back";
            this.btn_back.UseVisualStyleBackColor = true;
            this.btn_back.Click += new System.EventHandler(this.BackToMenu);
            // 
            // RoomMenuWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.btn_back);
            this.Controls.Add(this.lbl_error);
            this.Controls.Add(this.lbl_numplayers);
            this.Controls.Add(this.lstb_players);
            this.Controls.Add(this.btn_join);
            this.Controls.Add(this.btn_refresh);
            this.Controls.Add(this.lstb_rooms);
            this.Controls.Add(this.lbl_rooms);
            this.Name = "RoomMenuWindow";
            this.Text = "RoomMenuWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_rooms;
        private System.Windows.Forms.ListBox lstb_rooms;
        private System.Windows.Forms.Button btn_refresh;
        private System.Windows.Forms.Button btn_join;
        private System.Windows.Forms.ListBox lstb_players;
        private System.Windows.Forms.Label lbl_numplayers;
        private System.Windows.Forms.Label lbl_error;
        private System.Windows.Forms.Button btn_back;
    }
}