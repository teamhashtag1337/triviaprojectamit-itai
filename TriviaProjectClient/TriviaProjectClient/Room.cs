﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TriviaProjectClient
{
    public class Room
    {
        private int id;
        private string name;
        private int questionsNum;
        private int questionTime;
        private int maxPlayers;

        public int ID
        {
            get
            {
                return id;
            }

            set
            {
                if(id != value)
                {
                    id = value;
                }
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                if(name != value)
                {
                    name = value;
                }
            }
        }

        public int QuestionTime
        {
            get
            {
                return questionTime;
            }

            set
            {
                if(questionTime != value)
                {
                    questionTime = value;
                }
            }
        }

        public int QuestionsNum
        {
            get
            {
                return questionsNum;
            }

            set
            {
                if (questionsNum != value)
                {
                    questionsNum = value;
                }
            }
        }

        public int MaxPlayers
        {
            get
            {
                return maxPlayers;
            }

            set
            {
                if (maxPlayers != value)
                {
                    maxPlayers = value;
                }
            }
        }


        public Room(int id, string name)
        {
            this.id = id;
            this.name = name;
            this.questionsNum = 0;
            this.questionTime = 0;
            this.maxPlayers = 0;
        }


        public static Room ReadRoom()
        {

            /*
            NetworkStream clientStream = Config.socket;
            byte[] buffer = new byte[4];
            //Reading roomID
            int bytesRead = clientStream.Read(buffer, 0, 4); 
            string input = new ASCIIEncoding().GetString(buffer);
             * */

            //Reading roomID
            int roomId = int.Parse(Utils.ReadFromStream(4));

            //Reading room name
            /*
            buffer = new byte[2];
            bytesRead = clientStream.Read(buffer, 0, 2);
            input = new ASCIIEncoding().GetString(buffer);
             * */
            int nameLength = int.Parse(Utils.ReadFromStream(2));

            /*
            buffer = new byte[nameLength];
            bytesRead = clientStream.Read(buffer, 0, nameLength);
             */
     
            string roomName = Utils.ReadFromStream(nameLength);;

            return new Room(roomId, roomName);

        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
