﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaProjectClient
{
    public class Cryptor
    {
        public const int KEY_LENGTH = 3072;
        public const int KEY_LENGTH_BYTES = 384;
        public RSACryptoServiceProvider csp;
        public RSAParameters publicKey;
        public RSAParameters privateKey;
        public RSAParameters friendPublicKey;

        public Cryptor()
        {
            CspParameters parameters = new CspParameters();
            parameters.ProviderType = 1;
            parameters.Flags = CspProviderFlags.UseArchivableKey;
            parameters.KeyNumber = (int)KeyNumber.Exchange;

            friendPublicKey = new RSAParameters();
            csp = new RSACryptoServiceProvider(KEY_LENGTH, parameters);
            privateKey = csp.ExportParameters(true);
            publicKey = csp.ExportParameters(false);


            
        }

        public byte[] Encrypt(string message)
        {
            csp = new RSACryptoServiceProvider();
            csp.ImportParameters(friendPublicKey);

            byte[] bytesPlainTextData = new ASCIIEncoding().GetBytes(message);
            byte[] bytesCipherText = csp.Encrypt(bytesPlainTextData, false);

            return bytesCipherText;
        }
        
        public string Decrypt(byte[] message)
        {
            csp = new RSACryptoServiceProvider();
            csp.ImportParameters(privateKey);

            byte[] bytesPlainTextData = csp.Decrypt(message, false);

            return System.Text.Encoding.UTF8.GetString(bytesPlainTextData);
        }
    }
}
