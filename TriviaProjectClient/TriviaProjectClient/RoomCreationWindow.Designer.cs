﻿namespace TriviaProjectClient
{
    partial class RoomCreationWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_title = new System.Windows.Forms.Label();
            this.pnl_holder = new System.Windows.Forms.Panel();
            this.lbl_error = new System.Windows.Forms.Label();
            this.btn_create = new System.Windows.Forms.Button();
            this.txtb_questiontime = new System.Windows.Forms.TextBox();
            this.txtb_numquestions = new System.Windows.Forms.TextBox();
            this.txtb_numplayers = new System.Windows.Forms.TextBox();
            this.txtb_roomname = new System.Windows.Forms.TextBox();
            this.lbl_questiontime = new System.Windows.Forms.Label();
            this.lbl_numquestions = new System.Windows.Forms.Label();
            this.lbl_numplayers = new System.Windows.Forms.Label();
            this.lbl_roomname = new System.Windows.Forms.Label();
            this.btn_back = new System.Windows.Forms.Button();
            this.pnl_holder.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_title.Location = new System.Drawing.Point(155, 70);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(458, 76);
            this.lbl_title.TabIndex = 0;
            this.lbl_title.Text = "Create a room";
            // 
            // pnl_holder
            // 
            this.pnl_holder.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pnl_holder.Controls.Add(this.lbl_error);
            this.pnl_holder.Controls.Add(this.btn_create);
            this.pnl_holder.Controls.Add(this.txtb_questiontime);
            this.pnl_holder.Controls.Add(this.txtb_numquestions);
            this.pnl_holder.Controls.Add(this.txtb_numplayers);
            this.pnl_holder.Controls.Add(this.txtb_roomname);
            this.pnl_holder.Controls.Add(this.lbl_questiontime);
            this.pnl_holder.Controls.Add(this.lbl_numquestions);
            this.pnl_holder.Controls.Add(this.lbl_numplayers);
            this.pnl_holder.Controls.Add(this.lbl_roomname);
            this.pnl_holder.Location = new System.Drawing.Point(88, 176);
            this.pnl_holder.Name = "pnl_holder";
            this.pnl_holder.Size = new System.Drawing.Size(607, 348);
            this.pnl_holder.TabIndex = 1;
            // 
            // lbl_error
            // 
            this.lbl_error.AutoSize = true;
            this.lbl_error.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_error.ForeColor = System.Drawing.Color.Red;
            this.lbl_error.Location = new System.Drawing.Point(12, 0);
            this.lbl_error.Name = "lbl_error";
            this.lbl_error.Size = new System.Drawing.Size(113, 31);
            this.lbl_error.TabIndex = 7;
            this.lbl_error.Text = "lbl_error";
            this.lbl_error.Visible = false;
            // 
            // btn_create
            // 
            this.btn_create.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.btn_create.Location = new System.Drawing.Point(210, 276);
            this.btn_create.Name = "btn_create";
            this.btn_create.Size = new System.Drawing.Size(159, 69);
            this.btn_create.TabIndex = 2;
            this.btn_create.Text = "Create room";
            this.btn_create.UseVisualStyleBackColor = true;
            this.btn_create.Click += new System.EventHandler(this.CreateRoom);
            // 
            // txtb_questiontime
            // 
            this.txtb_questiontime.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtb_questiontime.Location = new System.Drawing.Point(279, 213);
            this.txtb_questiontime.Name = "txtb_questiontime";
            this.txtb_questiontime.Size = new System.Drawing.Size(325, 38);
            this.txtb_questiontime.TabIndex = 7;
            this.txtb_questiontime.Text = "5";
            // 
            // txtb_numquestions
            // 
            this.txtb_numquestions.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtb_numquestions.Location = new System.Drawing.Point(279, 153);
            this.txtb_numquestions.Name = "txtb_numquestions";
            this.txtb_numquestions.Size = new System.Drawing.Size(325, 38);
            this.txtb_numquestions.TabIndex = 6;
            this.txtb_numquestions.Text = "2";
            // 
            // txtb_numplayers
            // 
            this.txtb_numplayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtb_numplayers.Location = new System.Drawing.Point(279, 95);
            this.txtb_numplayers.Name = "txtb_numplayers";
            this.txtb_numplayers.Size = new System.Drawing.Size(325, 38);
            this.txtb_numplayers.TabIndex = 5;
            this.txtb_numplayers.Text = "2";
            // 
            // txtb_roomname
            // 
            this.txtb_roomname.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtb_roomname.Location = new System.Drawing.Point(279, 36);
            this.txtb_roomname.Name = "txtb_roomname";
            this.txtb_roomname.Size = new System.Drawing.Size(325, 38);
            this.txtb_roomname.TabIndex = 4;
            this.txtb_roomname.Text = "room_name";
            // 
            // lbl_questiontime
            // 
            this.lbl_questiontime.AutoSize = true;
            this.lbl_questiontime.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_questiontime.Location = new System.Drawing.Point(12, 220);
            this.lbl_questiontime.Name = "lbl_questiontime";
            this.lbl_questiontime.Size = new System.Drawing.Size(230, 31);
            this.lbl_questiontime.TabIndex = 3;
            this.lbl_questiontime.Text = "Time per question";
            // 
            // lbl_numquestions
            // 
            this.lbl_numquestions.AutoSize = true;
            this.lbl_numquestions.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_numquestions.Location = new System.Drawing.Point(12, 160);
            this.lbl_numquestions.Name = "lbl_numquestions";
            this.lbl_numquestions.Size = new System.Drawing.Size(264, 31);
            this.lbl_numquestions.TabIndex = 2;
            this.lbl_numquestions.Text = "Number of questions";
            // 
            // lbl_numplayers
            // 
            this.lbl_numplayers.AutoSize = true;
            this.lbl_numplayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_numplayers.Location = new System.Drawing.Point(12, 98);
            this.lbl_numplayers.Name = "lbl_numplayers";
            this.lbl_numplayers.Size = new System.Drawing.Size(235, 31);
            this.lbl_numplayers.TabIndex = 1;
            this.lbl_numplayers.Text = "Number of players";
            // 
            // lbl_roomname
            // 
            this.lbl_roomname.AutoSize = true;
            this.lbl_roomname.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_roomname.Location = new System.Drawing.Point(12, 43);
            this.lbl_roomname.Name = "lbl_roomname";
            this.lbl_roomname.Size = new System.Drawing.Size(160, 31);
            this.lbl_roomname.TabIndex = 0;
            this.lbl_roomname.Text = "Room name";
            // 
            // btn_back
            // 
            this.btn_back.Location = new System.Drawing.Point(663, 23);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(109, 60);
            this.btn_back.TabIndex = 2;
            this.btn_back.Text = "Back";
            this.btn_back.UseVisualStyleBackColor = true;
            this.btn_back.Click += new System.EventHandler(this.BackToMenu);
            // 
            // RoomCreationWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.btn_back);
            this.Controls.Add(this.pnl_holder);
            this.Controls.Add(this.lbl_title);
            this.Name = "RoomCreationWindow";
            this.Text = "RoomCreationWindow";
            this.pnl_holder.ResumeLayout(false);
            this.pnl_holder.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.Panel pnl_holder;
        private System.Windows.Forms.TextBox txtb_questiontime;
        private System.Windows.Forms.TextBox txtb_numquestions;
        private System.Windows.Forms.TextBox txtb_numplayers;
        private System.Windows.Forms.TextBox txtb_roomname;
        private System.Windows.Forms.Label lbl_questiontime;
        private System.Windows.Forms.Label lbl_numquestions;
        private System.Windows.Forms.Label lbl_numplayers;
        private System.Windows.Forms.Label lbl_roomname;
        private System.Windows.Forms.Button btn_create;
        private System.Windows.Forms.Button btn_back;
        private System.Windows.Forms.Label lbl_error;
    }
}