﻿namespace TriviaProjectClient
{
    partial class PersonalStatusWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {          
            this.lbl_title = new System.Windows.Forms.Label();
            this.pnl_scores = new System.Windows.Forms.Panel();
            this.lbl_thirdscore = new System.Windows.Forms.Label();
            this.lbl_secondscore = new System.Windows.Forms.Label();
            this.lbl_firstscore = new System.Windows.Forms.Label();
            this.lbl_third = new System.Windows.Forms.Label();
            this.lbl_second = new System.Windows.Forms.Label();
            this.lbl_first = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_avgtime = new System.Windows.Forms.Label();
            this.lbl_wrong = new System.Windows.Forms.Label();
            this.lbl_avgtimeval = new System.Windows.Forms.Label();
            this.lbl_wrongval = new System.Windows.Forms.Label();
            this.lbl_correctval = new System.Windows.Forms.Label();
            this.lbl_numCorrect = new System.Windows.Forms.Label();
            this.lbl_personalbest = new System.Windows.Forms.Label();
            this.lbl_status = new System.Windows.Forms.Label();
            this.btn_back = new System.Windows.Forms.Button();
            this.pnl_scores.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.BackColor = System.Drawing.Color.Transparent;
            this.lbl_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_title.Location = new System.Drawing.Point(115, 74);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(505, 76);
            this.lbl_title.TabIndex = 1;
            this.lbl_title.Text = "Personal Status";
            // 
            // pnl_scores
            // 
            this.pnl_scores.BackColor = System.Drawing.Color.Transparent;
            this.pnl_scores.Controls.Add(this.lbl_thirdscore);
            this.pnl_scores.Controls.Add(this.lbl_secondscore);
            this.pnl_scores.Controls.Add(this.lbl_firstscore);
            this.pnl_scores.Controls.Add(this.lbl_third);
            this.pnl_scores.Controls.Add(this.lbl_second);
            this.pnl_scores.Controls.Add(this.lbl_first);
            this.pnl_scores.Location = new System.Drawing.Point(12, 210);
            this.pnl_scores.Name = "pnl_scores";
            this.pnl_scores.Size = new System.Drawing.Size(363, 277);
            this.pnl_scores.TabIndex = 3;
            // 
            // lbl_thirdscore
            // 
            this.lbl_thirdscore.AutoSize = true;
            this.lbl_thirdscore.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_thirdscore.ForeColor = System.Drawing.Color.Black;
            this.lbl_thirdscore.Location = new System.Drawing.Point(113, 184);
            this.lbl_thirdscore.Name = "lbl_thirdscore";
            this.lbl_thirdscore.Size = new System.Drawing.Size(42, 46);
            this.lbl_thirdscore.TabIndex = 5;
            this.lbl_thirdscore.Text = "0";
            // 
            // lbl_secondscore
            // 
            this.lbl_secondscore.AutoSize = true;
            this.lbl_secondscore.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_secondscore.ForeColor = System.Drawing.Color.Black;
            this.lbl_secondscore.Location = new System.Drawing.Point(113, 110);
            this.lbl_secondscore.Name = "lbl_secondscore";
            this.lbl_secondscore.Size = new System.Drawing.Size(42, 46);
            this.lbl_secondscore.TabIndex = 4;
            this.lbl_secondscore.Text = "0";
            // 
            // lbl_firstscore
            // 
            this.lbl_firstscore.AutoSize = true;
            this.lbl_firstscore.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_firstscore.ForeColor = System.Drawing.Color.Black;
            this.lbl_firstscore.Location = new System.Drawing.Point(113, 28);
            this.lbl_firstscore.Name = "lbl_firstscore";
            this.lbl_firstscore.Size = new System.Drawing.Size(42, 46);
            this.lbl_firstscore.TabIndex = 3;
            this.lbl_firstscore.Text = "0";
            // 
            // lbl_third
            // 
            this.lbl_third.AutoSize = true;
            this.lbl_third.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_third.ForeColor = System.Drawing.Color.Black;
            this.lbl_third.Location = new System.Drawing.Point(29, 184);
            this.lbl_third.Name = "lbl_third";
            this.lbl_third.Size = new System.Drawing.Size(53, 46);
            this.lbl_third.TabIndex = 2;
            this.lbl_third.Text = "3.";
            // 
            // lbl_second
            // 
            this.lbl_second.AutoSize = true;
            this.lbl_second.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_second.ForeColor = System.Drawing.Color.Black;
            this.lbl_second.Location = new System.Drawing.Point(29, 110);
            this.lbl_second.Name = "lbl_second";
            this.lbl_second.Size = new System.Drawing.Size(53, 46);
            this.lbl_second.TabIndex = 1;
            this.lbl_second.Text = "2.";
            // 
            // lbl_first
            // 
            this.lbl_first.AutoSize = true;
            this.lbl_first.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_first.ForeColor = System.Drawing.Color.Black;
            this.lbl_first.Location = new System.Drawing.Point(29, 28);
            this.lbl_first.Name = "lbl_first";
            this.lbl_first.Size = new System.Drawing.Size(53, 46);
            this.lbl_first.TabIndex = 0;
            this.lbl_first.Text = "1.";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.lbl_avgtime);
            this.panel1.Controls.Add(this.lbl_wrong);
            this.panel1.Controls.Add(this.lbl_avgtimeval);
            this.panel1.Controls.Add(this.lbl_wrongval);
            this.panel1.Controls.Add(this.lbl_correctval);
            this.panel1.Controls.Add(this.lbl_numCorrect);
            this.panel1.Location = new System.Drawing.Point(409, 210);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(363, 277);
            this.panel1.TabIndex = 6;
            // 
            // lbl_avgtime
            // 
            this.lbl_avgtime.AutoSize = true;
            this.lbl_avgtime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_avgtime.ForeColor = System.Drawing.Color.Black;
            this.lbl_avgtime.Location = new System.Drawing.Point(15, 205);
            this.lbl_avgtime.Name = "lbl_avgtime";
            this.lbl_avgtime.Size = new System.Drawing.Size(227, 25);
            this.lbl_avgtime.TabIndex = 7;
            this.lbl_avgtime.Text = "Average answering time:";
            // 
            // lbl_wrong
            // 
            this.lbl_wrong.AutoSize = true;
            this.lbl_wrong.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_wrong.ForeColor = System.Drawing.Color.Black;
            this.lbl_wrong.Location = new System.Drawing.Point(18, 110);
            this.lbl_wrong.Name = "lbl_wrong";
            this.lbl_wrong.Size = new System.Drawing.Size(210, 31);
            this.lbl_wrong.TabIndex = 6;
            this.lbl_wrong.Text = "Wrong answers:";
            // 
            // lbl_avgtimeval
            // 
            this.lbl_avgtimeval.AutoSize = true;
            this.lbl_avgtimeval.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_avgtimeval.ForeColor = System.Drawing.Color.Black;
            this.lbl_avgtimeval.Location = new System.Drawing.Point(237, 188);
            this.lbl_avgtimeval.Name = "lbl_avgtimeval";
            this.lbl_avgtimeval.Size = new System.Drawing.Size(42, 46);
            this.lbl_avgtimeval.TabIndex = 5;
            this.lbl_avgtimeval.Text = "0";
            // 
            // lbl_wrongval
            // 
            this.lbl_wrongval.AutoSize = true;
            this.lbl_wrongval.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_wrongval.ForeColor = System.Drawing.Color.Black;
            this.lbl_wrongval.Location = new System.Drawing.Point(234, 105);
            this.lbl_wrongval.Name = "lbl_wrongval";
            this.lbl_wrongval.Size = new System.Drawing.Size(42, 46);
            this.lbl_wrongval.TabIndex = 4;
            this.lbl_wrongval.Text = "0";
            // 
            // lbl_correctval
            // 
            this.lbl_correctval.AutoSize = true;
            this.lbl_correctval.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_correctval.ForeColor = System.Drawing.Color.Black;
            this.lbl_correctval.Location = new System.Drawing.Point(234, 17);
            this.lbl_correctval.Name = "lbl_correctval";
            this.lbl_correctval.Size = new System.Drawing.Size(42, 46);
            this.lbl_correctval.TabIndex = 3;
            this.lbl_correctval.Text = "0";
            // 
            // lbl_numCorrect
            // 
            this.lbl_numCorrect.AutoSize = true;
            this.lbl_numCorrect.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_numCorrect.ForeColor = System.Drawing.Color.Black;
            this.lbl_numCorrect.Location = new System.Drawing.Point(14, 28);
            this.lbl_numCorrect.Name = "lbl_numCorrect";
            this.lbl_numCorrect.Size = new System.Drawing.Size(221, 31);
            this.lbl_numCorrect.TabIndex = 0;
            this.lbl_numCorrect.Text = "Correct answers:";
            // 
            // lbl_personalbest
            // 
            this.lbl_personalbest.AutoSize = true;
            this.lbl_personalbest.BackColor = System.Drawing.Color.Transparent;
            this.lbl_personalbest.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_personalbest.Location = new System.Drawing.Point(89, 176);
            this.lbl_personalbest.Name = "lbl_personalbest";
            this.lbl_personalbest.Size = new System.Drawing.Size(180, 31);
            this.lbl_personalbest.TabIndex = 7;
            this.lbl_personalbest.Text = "Personal best";
            // 
            // lbl_status
            // 
            this.lbl_status.AutoSize = true;
            this.lbl_status.BackColor = System.Drawing.Color.Transparent;
            this.lbl_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_status.Location = new System.Drawing.Point(546, 176);
            this.lbl_status.Name = "lbl_status";
            this.lbl_status.Size = new System.Drawing.Size(92, 31);
            this.lbl_status.TabIndex = 8;
            this.lbl_status.Text = "Status";
            // 
            // btn_back
            // 
            this.btn_back.Location = new System.Drawing.Point(654, 26);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(109, 60);
            this.btn_back.TabIndex = 9;
            this.btn_back.Text = "Back";
            this.btn_back.UseVisualStyleBackColor = true;
            this.btn_back.Click += new System.EventHandler(this.BackToMenu);
            // 
            // PersonalStatusWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.BackgroundImage = global::TriviaProjectClient.Properties.Resources.columnGraph_img;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.btn_back);
            this.Controls.Add(this.lbl_status);
            this.Controls.Add(this.lbl_personalbest);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnl_scores);
            this.Controls.Add(this.lbl_title);
            this.Name = "PersonalStatusWindow";
            this.Text = "PersonalStatusWindow";
            this.pnl_scores.ResumeLayout(false);
            this.pnl_scores.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.Panel pnl_scores;
        private System.Windows.Forms.Label lbl_thirdscore;
        private System.Windows.Forms.Label lbl_secondscore;
        private System.Windows.Forms.Label lbl_firstscore;
        private System.Windows.Forms.Label lbl_third;
        private System.Windows.Forms.Label lbl_second;
        private System.Windows.Forms.Label lbl_first;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_avgtimeval;
        private System.Windows.Forms.Label lbl_wrongval;
        private System.Windows.Forms.Label lbl_correctval;
        private System.Windows.Forms.Label lbl_numCorrect;
        private System.Windows.Forms.Label lbl_personalbest;
        private System.Windows.Forms.Label lbl_status;
        private System.Windows.Forms.Label lbl_wrong;
        private System.Windows.Forms.Label lbl_avgtime;
        private System.Windows.Forms.Button btn_back;
    }
}