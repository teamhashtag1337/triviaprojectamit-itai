﻿namespace TriviaProjectClient
{
    partial class BestScoresWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_title = new System.Windows.Forms.Label();
            this.pnl_scores = new System.Windows.Forms.Panel();
            this.lbl_thirdscore = new System.Windows.Forms.Label();
            this.lbl_secondscore = new System.Windows.Forms.Label();
            this.lbl_firstscore = new System.Windows.Forms.Label();
            this.lbl_third = new System.Windows.Forms.Label();
            this.lbl_second = new System.Windows.Forms.Label();
            this.lbl_first = new System.Windows.Forms.Label();
            this.btn_back = new System.Windows.Forms.Button();
            this.pnl_scores.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.BackColor = System.Drawing.Color.Transparent;
            this.lbl_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_title.Location = new System.Drawing.Point(148, 84);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(395, 76);
            this.lbl_title.TabIndex = 1;
            this.lbl_title.Text = "Best Scores";
            // 
            // pnl_scores
            // 
            this.pnl_scores.BackColor = System.Drawing.Color.Transparent;
            this.pnl_scores.Controls.Add(this.lbl_thirdscore);
            this.pnl_scores.Controls.Add(this.lbl_secondscore);
            this.pnl_scores.Controls.Add(this.lbl_firstscore);
            this.pnl_scores.Controls.Add(this.lbl_third);
            this.pnl_scores.Controls.Add(this.lbl_second);
            this.pnl_scores.Controls.Add(this.lbl_first);
            this.pnl_scores.Location = new System.Drawing.Point(103, 163);
            this.pnl_scores.Name = "pnl_scores";
            this.pnl_scores.Size = new System.Drawing.Size(543, 312);
            this.pnl_scores.TabIndex = 2;
            // 
            // lbl_thirdscore
            // 
            this.lbl_thirdscore.AutoSize = true;
            this.lbl_thirdscore.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_thirdscore.ForeColor = System.Drawing.Color.Black;
            this.lbl_thirdscore.Location = new System.Drawing.Point(113, 184);
            this.lbl_thirdscore.Name = "lbl_thirdscore";
            this.lbl_thirdscore.Size = new System.Drawing.Size(126, 46);
            this.lbl_thirdscore.TabIndex = 5;
            this.lbl_thirdscore.Text = "Score";
            // 
            // lbl_secondscore
            // 
            this.lbl_secondscore.AutoSize = true;
            this.lbl_secondscore.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_secondscore.ForeColor = System.Drawing.Color.Black;
            this.lbl_secondscore.Location = new System.Drawing.Point(113, 110);
            this.lbl_secondscore.Name = "lbl_secondscore";
            this.lbl_secondscore.Size = new System.Drawing.Size(126, 46);
            this.lbl_secondscore.TabIndex = 4;
            this.lbl_secondscore.Text = "Score";
            // 
            // lbl_firstscore
            // 
            this.lbl_firstscore.AutoSize = true;
            this.lbl_firstscore.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_firstscore.ForeColor = System.Drawing.Color.Black;
            this.lbl_firstscore.Location = new System.Drawing.Point(113, 41);
            this.lbl_firstscore.Name = "lbl_firstscore";
            this.lbl_firstscore.Size = new System.Drawing.Size(126, 46);
            this.lbl_firstscore.TabIndex = 3;
            this.lbl_firstscore.Text = "Score";
            // 
            // lbl_third
            // 
            this.lbl_third.AutoSize = true;
            this.lbl_third.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_third.ForeColor = System.Drawing.Color.Black;
            this.lbl_third.Location = new System.Drawing.Point(29, 184);
            this.lbl_third.Name = "lbl_third";
            this.lbl_third.Size = new System.Drawing.Size(53, 46);
            this.lbl_third.TabIndex = 2;
            this.lbl_third.Text = "3.";
            // 
            // lbl_second
            // 
            this.lbl_second.AutoSize = true;
            this.lbl_second.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_second.ForeColor = System.Drawing.Color.Black;
            this.lbl_second.Location = new System.Drawing.Point(29, 110);
            this.lbl_second.Name = "lbl_second";
            this.lbl_second.Size = new System.Drawing.Size(53, 46);
            this.lbl_second.TabIndex = 1;
            this.lbl_second.Text = "2.";
            // 
            // lbl_first
            // 
            this.lbl_first.AutoSize = true;
            this.lbl_first.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_first.ForeColor = System.Drawing.Color.Black;
            this.lbl_first.Location = new System.Drawing.Point(29, 41);
            this.lbl_first.Name = "lbl_first";
            this.lbl_first.Size = new System.Drawing.Size(53, 46);
            this.lbl_first.TabIndex = 0;
            this.lbl_first.Text = "1.";
            // 
            // btn_back
            // 
            this.btn_back.Location = new System.Drawing.Point(650, 34);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(109, 60);
            this.btn_back.TabIndex = 8;
            this.btn_back.Text = "Back";
            this.btn_back.UseVisualStyleBackColor = true;
            this.btn_back.Click += new System.EventHandler(this.BackToMenu);
            // 
            // BestScoresWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TriviaProjectClient.Properties.Resources.victoryCup_img;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.btn_back);
            this.Controls.Add(this.pnl_scores);
            this.Controls.Add(this.lbl_title);
            this.Name = "BestScoresWindow";
            this.Text = "BestScoresWindow";
            this.pnl_scores.ResumeLayout(false);
            this.pnl_scores.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.Panel pnl_scores;
        private System.Windows.Forms.Label lbl_thirdscore;
        private System.Windows.Forms.Label lbl_secondscore;
        private System.Windows.Forms.Label lbl_firstscore;
        private System.Windows.Forms.Label lbl_third;
        private System.Windows.Forms.Label lbl_second;
        private System.Windows.Forms.Label lbl_first;
        private System.Windows.Forms.Button btn_back;
    }
}