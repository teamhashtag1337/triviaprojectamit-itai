﻿namespace TriviaProjectClient
{
    partial class ChatRoomWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtb_chat = new System.Windows.Forms.TextBox();
            this.txtb_message = new System.Windows.Forms.TextBox();
            this.btn_send = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtb_chat
            // 
            this.txtb_chat.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtb_chat.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtb_chat.HideSelection = false;
            this.txtb_chat.Location = new System.Drawing.Point(1, 12);
            this.txtb_chat.Multiline = true;
            this.txtb_chat.Name = "txtb_chat";
            this.txtb_chat.ReadOnly = true;
            this.txtb_chat.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtb_chat.ShortcutsEnabled = false;
            this.txtb_chat.Size = new System.Drawing.Size(250, 163);
            this.txtb_chat.TabIndex = 0;
            // 
            // txtb_message
            // 
            this.txtb_message.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.txtb_message.Location = new System.Drawing.Point(1, 202);
            this.txtb_message.MaxLength = 9999;
            this.txtb_message.Multiline = true;
            this.txtb_message.Name = "txtb_message";
            this.txtb_message.Size = new System.Drawing.Size(172, 26);
            this.txtb_message.TabIndex = 1;
            this.txtb_message.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtb_message_KeyPress);
            // 
            // btn_send
            // 
            this.btn_send.Location = new System.Drawing.Point(190, 200);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(61, 28);
            this.btn_send.TabIndex = 2;
            this.btn_send.Text = "Send";
            this.btn_send.UseVisualStyleBackColor = true;
            this.btn_send.Click += new System.EventHandler(this.sendToServer);
            // 
            // ChatRoomWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(263, 258);
            this.Controls.Add(this.btn_send);
            this.Controls.Add(this.txtb_message);
            this.Controls.Add(this.txtb_chat);
            this.Name = "ChatRoomWindow";
            this.Text = "ChatRoomWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtb_chat;
        private System.Windows.Forms.TextBox txtb_message;
        private System.Windows.Forms.Button btn_send;
    }
}