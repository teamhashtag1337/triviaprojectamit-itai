﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaProjectClient
{
    public partial class BestScoresWindow : Form
    {
        private NetworkStream clientStream;

        public BestScoresWindow()
        {
            InitializeComponent();
            this.clientStream = Config.socket;
            RequestScores();
        }

        private void RequestScores()
        {
            string message = Protocol.USR_HIGHSCORE.ToString();
            int[] scores = new int[3];
            string[] users = new string[3];

            //Send message

            Utils.SendEncrypted(message);
            /*
            byte[] buffer = new ASCIIEncoding().GetBytes(message);
            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
            */


            //Recieve message

            Utils.ReadEncrypted();
            
            //Recieve message code

            /*
            buffer = new byte[3];
            int bytesRead = clientStream.Read(buffer, 0, 3);
            */
            Utils.ReadFromStream(3);

            for(int i = 0; i < 3; i++)
            {
                //Recieving username length

                /*
                buffer = new byte[2];
                bytesRead = clientStream.Read(buffer, 0, 2);
                string input = new ASCIIEncoding().GetString(buffer);
                 */
                int usernameLength = int.Parse(Utils.ReadFromStream(2));

                //Recieving username

                /*
                buffer = new byte[usernameLength];
                bytesRead = clientStream.Read(buffer, 0, usernameLength);
                input = new ASCIIEncoding().GetString(buffer);
                 * */

                string username = Utils.ReadFromStream(usernameLength);

                //Reciving number of correct answers

                /*
                buffer = new byte[6];
                bytesRead = clientStream.Read(buffer, 0, 6);
                input = new ASCIIEncoding().GetString(buffer);
                 * */
                int score = int.Parse(Utils.ReadFromStream(6));

                scores[i] = score;
                users[i] = username;
            }

            lbl_firstscore.Text = users[0] + "    " + scores[0].ToString();
            lbl_secondscore.Text = users[1] + "    " + scores[1].ToString();
            lbl_thirdscore.Text = users[2] + "    " + scores[2].ToString();

        }

        private void BackToMenu(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
