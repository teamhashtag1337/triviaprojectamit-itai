﻿namespace TriviaProjectClient
{
    partial class MainMenuWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_joinroom = new System.Windows.Forms.Button();
            this.btn_createroom = new System.Windows.Forms.Button();
            this.btn_mystatus = new System.Windows.Forms.Button();
            this.btn_bestscores = new System.Windows.Forms.Button();
            this.btn_signup = new System.Windows.Forms.Button();
            this.btn_exit = new System.Windows.Forms.Button();
            this.pnl_login = new System.Windows.Forms.Panel();
            this.lbl_hello = new System.Windows.Forms.Label();
            this.lbl_error = new System.Windows.Forms.Label();
            this.lbl_pass = new System.Windows.Forms.Label();
            this.lbl_uname = new System.Windows.Forms.Label();
            this.txtb_pass = new System.Windows.Forms.TextBox();
            this.txtb_uname = new System.Windows.Forms.TextBox();
            this.btn_signin = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnl_login.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_joinroom
            // 
            this.btn_joinroom.Enabled = false;
            this.btn_joinroom.Location = new System.Drawing.Point(243, 244);
            this.btn_joinroom.Name = "btn_joinroom";
            this.btn_joinroom.Size = new System.Drawing.Size(277, 61);
            this.btn_joinroom.TabIndex = 0;
            this.btn_joinroom.Text = "Join Room";
            this.btn_joinroom.UseVisualStyleBackColor = true;
            this.btn_joinroom.Click += new System.EventHandler(this.JoinRoom);
            // 
            // btn_createroom
            // 
            this.btn_createroom.Enabled = false;
            this.btn_createroom.Location = new System.Drawing.Point(243, 311);
            this.btn_createroom.Name = "btn_createroom";
            this.btn_createroom.Size = new System.Drawing.Size(277, 61);
            this.btn_createroom.TabIndex = 1;
            this.btn_createroom.Text = "Create Room";
            this.btn_createroom.UseVisualStyleBackColor = true;
            this.btn_createroom.Click += new System.EventHandler(this.CreateRoom);
            // 
            // btn_mystatus
            // 
            this.btn_mystatus.Enabled = false;
            this.btn_mystatus.Location = new System.Drawing.Point(243, 378);
            this.btn_mystatus.Name = "btn_mystatus";
            this.btn_mystatus.Size = new System.Drawing.Size(277, 61);
            this.btn_mystatus.TabIndex = 2;
            this.btn_mystatus.Text = "My Status";
            this.btn_mystatus.UseVisualStyleBackColor = true;
            this.btn_mystatus.Click += new System.EventHandler(this.MyStatus);
            // 
            // btn_bestscores
            // 
            this.btn_bestscores.Enabled = false;
            this.btn_bestscores.Location = new System.Drawing.Point(243, 445);
            this.btn_bestscores.Name = "btn_bestscores";
            this.btn_bestscores.Size = new System.Drawing.Size(277, 61);
            this.btn_bestscores.TabIndex = 3;
            this.btn_bestscores.Text = "Best Scores";
            this.btn_bestscores.UseVisualStyleBackColor = true;
            this.btn_bestscores.Click += new System.EventHandler(this.BestScores);
            // 
            // btn_signup
            // 
            this.btn_signup.Location = new System.Drawing.Point(243, 177);
            this.btn_signup.Name = "btn_signup";
            this.btn_signup.Size = new System.Drawing.Size(277, 61);
            this.btn_signup.TabIndex = 4;
            this.btn_signup.Text = "Sign up";
            this.btn_signup.UseVisualStyleBackColor = true;
            this.btn_signup.Click += new System.EventHandler(this.SignUp);
            // 
            // btn_exit
            // 
            this.btn_exit.Location = new System.Drawing.Point(339, 513);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(81, 39);
            this.btn_exit.TabIndex = 5;
            this.btn_exit.Text = "exit";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.Exit);
            // 
            // pnl_login
            // 
            this.pnl_login.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pnl_login.Controls.Add(this.lbl_hello);
            this.pnl_login.Controls.Add(this.lbl_error);
            this.pnl_login.Controls.Add(this.lbl_pass);
            this.pnl_login.Controls.Add(this.lbl_uname);
            this.pnl_login.Controls.Add(this.txtb_pass);
            this.pnl_login.Controls.Add(this.txtb_uname);
            this.pnl_login.Controls.Add(this.btn_signin);
            this.pnl_login.Location = new System.Drawing.Point(41, 71);
            this.pnl_login.Name = "pnl_login";
            this.pnl_login.Size = new System.Drawing.Size(705, 100);
            this.pnl_login.TabIndex = 6;
            // 
            // lbl_hello
            // 
            this.lbl_hello.AutoSize = true;
            this.lbl_hello.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lbl_hello.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_hello.Location = new System.Drawing.Point(191, 19);
            this.lbl_hello.Name = "lbl_hello";
            this.lbl_hello.Size = new System.Drawing.Size(229, 63);
            this.lbl_hello.TabIndex = 14;
            this.lbl_hello.Text = "lbl_hello";
            this.lbl_hello.Visible = false;
            // 
            // lbl_error
            // 
            this.lbl_error.AutoSize = true;
            this.lbl_error.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_error.ForeColor = System.Drawing.Color.Red;
            this.lbl_error.Location = new System.Drawing.Point(190, 82);
            this.lbl_error.Name = "lbl_error";
            this.lbl_error.Size = new System.Drawing.Size(66, 20);
            this.lbl_error.TabIndex = 13;
            this.lbl_error.Text = "lbl_error";
            this.lbl_error.Visible = false;
            // 
            // lbl_pass
            // 
            this.lbl_pass.AutoSize = true;
            this.lbl_pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_pass.Location = new System.Drawing.Point(32, 57);
            this.lbl_pass.Name = "lbl_pass";
            this.lbl_pass.Size = new System.Drawing.Size(98, 25);
            this.lbl_pass.TabIndex = 11;
            this.lbl_pass.Text = "Password";
            // 
            // lbl_uname
            // 
            this.lbl_uname.AutoSize = true;
            this.lbl_uname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_uname.Location = new System.Drawing.Point(32, 23);
            this.lbl_uname.Name = "lbl_uname";
            this.lbl_uname.Size = new System.Drawing.Size(110, 25);
            this.lbl_uname.TabIndex = 10;
            this.lbl_uname.Text = "User Name";
            // 
            // txtb_pass
            // 
            this.txtb_pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtb_pass.Location = new System.Drawing.Point(192, 54);
            this.txtb_pass.Name = "txtb_pass";
            this.txtb_pass.PasswordChar = '*';
            this.txtb_pass.Size = new System.Drawing.Size(342, 30);
            this.txtb_pass.TabIndex = 9;
            // 
            // txtb_uname
            // 
            this.txtb_uname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtb_uname.Location = new System.Drawing.Point(192, 18);
            this.txtb_uname.Name = "txtb_uname";
            this.txtb_uname.Size = new System.Drawing.Size(342, 30);
            this.txtb_uname.TabIndex = 8;
            // 
            // btn_signin
            // 
            this.btn_signin.Location = new System.Drawing.Point(550, 19);
            this.btn_signin.Name = "btn_signin";
            this.btn_signin.Size = new System.Drawing.Size(137, 61);
            this.btn_signin.TabIndex = 7;
            this.btn_signin.Text = "Sign in";
            this.btn_signin.UseVisualStyleBackColor = true;
            this.btn_signin.Click += new System.EventHandler(this.SignInOut);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::TriviaProjectClient.Properties.Resources.trivia_img;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(144, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(505, 63);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // MainMenuWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(137)))), ((int)(((byte)(51)))));
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pnl_login);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.btn_signup);
            this.Controls.Add(this.btn_bestscores);
            this.Controls.Add(this.btn_mystatus);
            this.Controls.Add(this.btn_createroom);
            this.Controls.Add(this.btn_joinroom);
            this.Name = "MainMenuWindow";
            this.Text = "Trivia";
            this.pnl_login.ResumeLayout(false);
            this.pnl_login.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_joinroom;
        private System.Windows.Forms.Button btn_createroom;
        private System.Windows.Forms.Button btn_mystatus;
        private System.Windows.Forms.Button btn_bestscores;
        private System.Windows.Forms.Button btn_signup;
        private System.Windows.Forms.Button btn_exit;
        private System.Windows.Forms.Panel pnl_login;
        private System.Windows.Forms.Label lbl_uname;
        private System.Windows.Forms.TextBox txtb_pass;
        private System.Windows.Forms.TextBox txtb_uname;
        private System.Windows.Forms.Button btn_signin;
        private System.Windows.Forms.Label lbl_pass;
        private System.Windows.Forms.Label lbl_error;
        private System.Windows.Forms.Label lbl_hello;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

