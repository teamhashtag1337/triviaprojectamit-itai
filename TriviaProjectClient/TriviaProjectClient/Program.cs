﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaProjectClient
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.ApplicationExit += ExitApp;
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Config.stringStream = Utils.GenerateStream("");
                Application.Run(new ConfigWindow());
                 

            }

            catch(Exception e)
            {
                MessageBox.Show("Failed to connect to the server. Please restart the client.");
            }

        }

        private static void ExitApp(object sender, EventArgs e)
        {
            NetworkStream clientStream = Config.socket;
            if(clientStream != null)
            {
                string message = Protocol.USR_LOGOUT.ToString(); //Log out
                Utils.SendEncrypted(message);
                /*
                byte[] buffer = new ASCIIEncoding().GetBytes(message);
                clientStream.Write(buffer, 0, message.Length);
                clientStream.Flush();
                 * */

                clientStream.Close();
            }

            if(Config.stringStream != null)
            {
                Config.stringStream.Dispose();
            }
 
        }


    }
}
