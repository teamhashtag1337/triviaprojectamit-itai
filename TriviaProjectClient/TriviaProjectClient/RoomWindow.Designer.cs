﻿namespace TriviaProjectClient
{
    partial class RoomWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_title = new System.Windows.Forms.Label();
            this.lbl_maxplayers = new System.Windows.Forms.Label();
            this.lbl_numquestions = new System.Windows.Forms.Label();
            this.lbl_timeperquestion = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_error = new System.Windows.Forms.Label();
            this.lstb_players = new System.Windows.Forms.ListBox();
            this.lbl_connectedusers = new System.Windows.Forms.Label();
            this.btn_leave = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_title.Location = new System.Drawing.Point(200, 0);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(238, 76);
            this.lbl_title.TabIndex = 0;
            this.lbl_title.Text = "lbl_title";
            // 
            // lbl_maxplayers
            // 
            this.lbl_maxplayers.AutoSize = true;
            this.lbl_maxplayers.BackColor = System.Drawing.SystemColors.Control;
            this.lbl_maxplayers.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_maxplayers.Location = new System.Drawing.Point(76, 75);
            this.lbl_maxplayers.Name = "lbl_maxplayers";
            this.lbl_maxplayers.Size = new System.Drawing.Size(131, 25);
            this.lbl_maxplayers.TabIndex = 1;
            this.lbl_maxplayers.Text = "Max Players: ";
            this.lbl_maxplayers.Visible = false;
            // 
            // lbl_numquestions
            // 
            this.lbl_numquestions.AutoSize = true;
            this.lbl_numquestions.BackColor = System.Drawing.SystemColors.HighlightText;
            this.lbl_numquestions.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_numquestions.Location = new System.Drawing.Point(278, 149);
            this.lbl_numquestions.Name = "lbl_numquestions";
            this.lbl_numquestions.Size = new System.Drawing.Size(202, 25);
            this.lbl_numquestions.TabIndex = 2;
            this.lbl_numquestions.Text = "Number of questions: ";
            // 
            // lbl_timeperquestion
            // 
            this.lbl_timeperquestion.AutoSize = true;
            this.lbl_timeperquestion.BackColor = System.Drawing.SystemColors.HighlightText;
            this.lbl_timeperquestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_timeperquestion.Location = new System.Drawing.Point(495, 149);
            this.lbl_timeperquestion.Name = "lbl_timeperquestion";
            this.lbl_timeperquestion.Size = new System.Drawing.Size(174, 25);
            this.lbl_timeperquestion.TabIndex = 3;
            this.lbl_timeperquestion.Text = "Time per question:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightBlue;
            this.panel1.Controls.Add(this.lbl_error);
            this.panel1.Controls.Add(this.lstb_players);
            this.panel1.Controls.Add(this.lbl_maxplayers);
            this.panel1.Controls.Add(this.lbl_title);
            this.panel1.Controls.Add(this.lbl_connectedusers);
            this.panel1.Location = new System.Drawing.Point(42, 74);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(686, 380);
            this.panel1.TabIndex = 4;
            // 
            // lbl_error
            // 
            this.lbl_error.AutoSize = true;
            this.lbl_error.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_error.Location = new System.Drawing.Point(30, 334);
            this.lbl_error.Name = "lbl_error";
            this.lbl_error.Size = new System.Drawing.Size(166, 46);
            this.lbl_error.TabIndex = 6;
            this.lbl_error.Text = "lbl_error";
            this.lbl_error.Visible = false;
            // 
            // lstb_players
            // 
            this.lstb_players.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lstb_players.FormattingEnabled = true;
            this.lstb_players.ItemHeight = 31;
            this.lstb_players.Location = new System.Drawing.Point(196, 174);
            this.lstb_players.Name = "lstb_players";
            this.lstb_players.Size = new System.Drawing.Size(277, 128);
            this.lstb_players.TabIndex = 5;
            // 
            // lbl_connectedusers
            // 
            this.lbl_connectedusers.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.lbl_connectedusers.AutoSize = true;
            this.lbl_connectedusers.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl_connectedusers.Location = new System.Drawing.Point(173, 110);
            this.lbl_connectedusers.Name = "lbl_connectedusers";
            this.lbl_connectedusers.Size = new System.Drawing.Size(332, 46);
            this.lbl_connectedusers.TabIndex = 0;
            this.lbl_connectedusers.Text = "Users connected:";
            // 
            // btn_leave
            // 
            this.btn_leave.BackColor = System.Drawing.SystemColors.Control;
            this.btn_leave.Location = new System.Drawing.Point(299, 460);
            this.btn_leave.Name = "btn_leave";
            this.btn_leave.Size = new System.Drawing.Size(138, 80);
            this.btn_leave.TabIndex = 5;
            this.btn_leave.Text = "Leave Room";
            this.btn_leave.UseVisualStyleBackColor = false;
            this.btn_leave.Click += new System.EventHandler(this.LeaveRoom);
            // 
            // RoomWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.btn_leave);
            this.Controls.Add(this.lbl_numquestions);
            this.Controls.Add(this.lbl_timeperquestion);
            this.Controls.Add(this.panel1);
            this.Name = "RoomWindow";
            this.Text = "RoomWindow";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.Label lbl_maxplayers;
        private System.Windows.Forms.Label lbl_numquestions;
        private System.Windows.Forms.Label lbl_timeperquestion;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_connectedusers;
        private System.Windows.Forms.ListBox lstb_players;
        private System.Windows.Forms.Button btn_leave;
        private System.Windows.Forms.Label lbl_error;
    }
}