﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaProjectClient
{
    public partial class PersonalStatusWindow : Form
    {
        private NetworkStream clientStream;

        public PersonalStatusWindow()
        {
            InitializeComponent();
            this.clientStream = Config.socket;
            RequestScores();
        }

        private void RequestScores()
        {
            string message = Protocol.USR_PERSONAL_STATUS.ToString();
            int[] scores = new int[3];
            string[] users = new string[3];

            //Send message

            /*
            byte[] buffer = new ASCIIEncoding().GetBytes(message);
            clientStream.Write(buffer, 0, message.Length);
            clientStream.Flush();
            */

            Utils.SendEncrypted(message);

            Utils.ReadEncrypted();
            //Recieve message code
            /*
            buffer = new byte[3];
            int bytesRead = clientStream.Read(buffer, 0, 3);
            */

            Utils.ReadFromStream(3);

            //Recieve number of games

            /*
            buffer = new byte[4];
            bytesRead = clientStream.Read(buffer, 0, 4);
             * */
            string input = Utils.ReadFromStream(4);

            if(int.Parse(input) == 0)
            {
                return;
            }


            //Recieve number of correct answers
            /*
            buffer = new byte[6];
            bytesRead = clientStream.Read(buffer, 0, 6);
             * */
            input = Utils.ReadFromStream(6);
            lbl_correctval.Text = int.Parse(input).ToString(); //In order to get rid of the 6 byte format

            //Recieve number of wrong answers
            /*
            buffer = new byte[6];
            bytesRead = clientStream.Read(buffer, 0, 6);
             * */
            input = Utils.ReadFromStream(6);
            lbl_wrongval.Text = int.Parse(input).ToString(); //In order to get rid of the 6 byte format

            //Recieve average time
            /*
            buffer = new byte[4];
            bytesRead = clientStream.Read(buffer, 0, 4);
             * */
            double val = double.Parse( Utils.ReadFromStream(4)); //In order to get rid of the 4 byte format
            val /= 100;
            lbl_avgtimeval.Text = val.ToString();

        }


        private void BackToMenu(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
