﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaProjectClient
{
    public partial class ChatRoomWindow : Form
    {
        private NetworkStream clientStream;

        public ChatRoomWindow()
        {
            InitializeComponent();
            clientStream = Config.socket;
            SendMessage("You have joined!\n");
        }


        public void DigestMessage()
        {

            /*
            byte[] buffer = new byte[1];

            //Reading number of players
            int bytesRead = clientStream.Read(buffer, 0, 1);
            string input = new ASCIIEncoding().GetString(buffer);
             * */

            int msgCode = int.Parse(Utils.ReadFromStream(1));


            //Reading username
            /*
            buffer = new byte[2];
            bytesRead = clientStream.Read(buffer, 0, 2);
            input = new ASCIIEncoding().GetString(buffer);
             * */
            int nameLength = int.Parse(Utils.ReadFromStream(2));

            /*
            buffer = new byte[nameLength];
            bytesRead = clientStream.Read(buffer, 0, nameLength);
             * */
            string username = Utils.ReadFromStream(nameLength);
            

            switch(msgCode)
            {
                case 0:

                    //Reading message

                    /*
                   buffer = new byte[4];
                   bytesRead = clientStream.Read(buffer, 0, 4);
                   input = new ASCIIEncoding().GetString(buffer);
                     * */
                   int messageLength = int.Parse(Utils.ReadFromStream(4));


                    /*
                   buffer = new byte[messageLength];
                   bytesRead = clientStream.Read(buffer, 0, messageLength);
                     * */
                   string message = Utils.ReadFromStream(messageLength);

                   Invoke((MethodInvoker)delegate { SendUserMessage(username, message); });
                     
                    break;
                case 1:
                    Invoke((MethodInvoker)delegate { SendConnected(username); });
                    break;
                case 2:
                    Invoke((MethodInvoker)delegate { SendDisconnected(username); });
                    
                    break;
            }
        }     

        public void SendConnected(string user)
        {
            SendMessage("Server: " + user + " has joined!");
        }

        public void SendDisconnected(string user)
        {
            SendMessage("Server: " + user + " has left!");
        }

        public void SendUserMessage(string user, string message)
        {
                SendMessage(user + ": " + message);
        }

        private void SendMessage(string message)
        {
            txtb_chat.AppendText(message);
            txtb_chat.AppendText(Environment.NewLine);
        }

        private void sendToServer(object sender, EventArgs e)
        {
            if(txtb_message.Text != "" && txtb_message.Text != "\r\n") //If the message is not empty
            {
                string message = Protocol.USR_CHATROOM_MSG.ToString();
                message += Utils.ParseIntToXBytes(Config.username.Length, 2);
                message += Config.username;
                message += Utils.ParseIntToXBytes(txtb_message.Text.Length, 4);
                message += txtb_message.Text;


                /*
                //Send message
                byte[] buffer = new ASCIIEncoding().GetBytes(message);
                clientStream.Write(buffer, 0, message.Length);
                clientStream.Flush();
                 * */

                Utils.SendEncrypted(message);
            }

            txtb_message.Text = "";
        }

        private void txtb_message_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == (char)Keys.Return) //Send the message if the user press enter
            {
                txtb_message.Text = txtb_message.Text.Replace("\r\n", "");
                sendToServer(null, null);
            }
        }

    }
}
