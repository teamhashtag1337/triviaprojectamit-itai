#include "User.h"

User::User(string name, SOCKET sock) : _currRoom(nullptr), _currGame(nullptr)
{
	_username = name;
	_sock = sock;
}

void User::send(string message)
{
	Helper::sendData(_sock, message, _cryptor);
}

string User::getUserName()
{
	return _username;
}

SOCKET User::getSocket()
{
	return _sock;
}

Game* User::getGame()
{
	return _currGame;
}

void User::setGame(Game* gm)
{
	_currGame = gm;
	_currRoom = nullptr;
}

void User::clearGame()
{
	_currGame = nullptr;
}

void User::setRoom(Room* room)
{
	_currRoom = room;
}

void User::setCryptor(Cryptor* cryptor)
{
	_cryptor = cryptor;
}

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (_currRoom != nullptr) //If the user is already in a room
	{
		send("1141"); //Failiure to create a new room
		return false;
	}
	else
	{
		Room* room = new Room(roomId, this, roomName, maxUsers, questionTime, questionsNo); //Creating a new room with the current player as admin
		_currRoom = room;
		send("1140"); //Success in creating a new room
		return true;
	}
}

bool User::joinRoom(Room* newRoom)
{
	if (_currRoom != nullptr) //If the user is already in a room
		return false;
	else
		return (newRoom->joinRoom(this));
}

void User::leaveRoom()
{
	if (_currRoom != nullptr) //If the user is already in a room
	{
		_currRoom->leaveRoom(this);
		_currRoom = nullptr;
	}
}

int User::closeRoom()
{
	if (_currRoom == nullptr) //If the user is not in a room
		return -1;
	else
	{
		_currRoom->closeRoom(this);
		delete _currRoom;
		_currRoom = nullptr;
		return 0;
	}
}

bool User::leaveGame()
{
	if (_currGame != nullptr) //If the user is currently in a game
	{
		_currGame->leaveGame(this);
		_currGame = nullptr;
		return true;
	}

	return false;
}

void User::clearRoom()
{
	_currRoom = nullptr;
}

bool User::operator==(User* other)
{
	return (_username == other->_username);
}

Room* User::getRoom()
{
	return _currRoom;
}