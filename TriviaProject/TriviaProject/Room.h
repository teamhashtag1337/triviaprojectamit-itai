#pragma once

#include "stdafx.h"
#include <vector>
#include <algorithm>
#include "User.h"

class User;

class Room
{
private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionsNo;
	string _name;
	int _id;


	string getUsersAsString(vector<User*> usersList, User* excludeUser);
	

public:
	Room(int id, User* admin, string name, int maxUsers, int questionTime, int questionsNo);
	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuestionsNo();
	int getQuestionsTime();
	int getId();
	string getName();
	void sendMessage(string message);
	void sendMessage(User* excludeUser, string message);

};