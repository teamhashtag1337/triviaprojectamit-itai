#pragma once

#include <queue>
#include "User.h"
#include <map>
#include <WinSock2.h>
#include <Windows.h>
#include "RecievedMessage.h"
#include "DataBase.h"
#include "Validator.h"
#include "stdafx.h"
#include <exception>
#include <mutex>
#include <condition_variable>
#include <fstream>
#include <thread>
#include "Protocol.h"
#include <map>
#include <algorithm>
#include "Helper.h"



class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();
	void serve(int port);

private:
	void accept();
	void clientHandler(SOCKET clientSocket);
	void updateClientFile(User* user);
	void handleRecievedMessages(); //Updates the messages queue
	void addRecievedMessage(RecievedMessage* message);
	void buildRecievedMessage(SOCKET socket, int msgcode);
	void buildRecievedMessage(SOCKET socket, int msgcode, stringstream& message);
	void disconnectUser(User* user); //Disconnects a certain user from the server
	void waitToRead(RecievedMessage& message); //Waits for the message entered to be completely read

	bool handleSignup(RecievedMessage* message);
	User* handleSignin(RecievedMessage* message);
	void handleSignout(RecievedMessage* message);
	void handleGetRooms(RecievedMessage* message);
	User* getUserByName(string username);
	void handleGetUsersInRoom(RecievedMessage* message);
	bool handleCreateRoom(RecievedMessage* message);
	User* getUserBySocket(SOCKET socket);
	bool handleCloseRoom(RecievedMessage* message);
	Room* getRoomById(int id);
	void safeDeleteUser(RecievedMessage* message);
	bool handleJoinRoom(RecievedMessage* message);
	bool handleLeaveRoom(RecievedMessage* message);
	void handleStartGame(RecievedMessage* message);
	void handlePlayerAnswer(RecievedMessage* message);
	void handleLeaveGame(RecievedMessage* message);
	void handleBestScores(RecievedMessage* message);
	void handleGetPersonalStatus(RecievedMessage* message);
	void handleChatMessage(RecievedMessage* message);
	void handleKeyRecieve(RecievedMessage* message);
	string buildMessage(int code, string message, string username);
	void sendKeyToClient(SOCKET clientSocket);
	Cryptor* getCryptorByUser(User* user);
	Cryptor* getCryptorBySocket(SOCKET socket);
	void safeDeleteConnection(RecievedMessage* message);
	void waitForAuth();

	SOCKET _serverSocket;
	std::map<SOCKET, User*> _connectedUsers;
	std::map<SOCKET, Cryptor*> _connections;
	DataBase* _db;
	User* _currentUser;
	std::queue<RecievedMessage*> _messages;
	static int _roomIdSequence;
	map<int, Room*> _roomsList;
	Cryptor _cryptor;
};



