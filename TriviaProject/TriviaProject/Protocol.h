#pragma once

class Protocol
{
public:
	static const int USR_LOG_IN = 200;
	static const int USR_LOG_OUT = 201;
	static const int SRV_LOG_IN = 102;
	static const int USR_SIGN_UP = 203;
	static const int SRV_USR_LIST = 108;
	static const int USR_EXIT = 299;
	static const int USR_ROOM_LIST = 205;
	static const int USR_ROOM_USRLIST = 207;
	static const int USR_ROOM_JOIN = 209;
	static const int USR_ROOM_LEAVE = 211;
	static const int USR_ROOM_CREATE = 213;
	static const int USR_ROOM_CLOSE = 215;
	static const int USR_GAME_START = 217;
	static const int USR_GAME_PLAYER_ANS = 219;
	static const int USR_GAME_LEAVE = 222;
	static const int USR_BEST_SCORES = 223;
	static const int USR_PERSONAL_STATUS = 225;
	static const int USR_CHATROOM_MSG = 300;
	static const int SRV_CHATROOM_MSG = 301;
	static const int USR_ENCRYPTION_RES = 400;
	static const int SRV_ENCRYPTION_REQ = 401;
};
