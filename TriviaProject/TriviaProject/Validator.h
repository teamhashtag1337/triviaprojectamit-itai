#pragma once

#include "stdafx.h"
#include <algorithm>

class Validator
{
public:
	static bool isPasswordValid(string password); //Returns true if the password is valid else false
	static bool isUsernameValid(string username); //Returns true if the username is valid else false

private:
	static bool isLetter(char c); //Returns true if the char is a letter else false
	static bool isNumber(char c); //Returns true if the char is a number else false
	static bool isSmallLetter(char c); //Returns true if the char is a small letter else false
	static bool isCapitalLetter(char c); //Returns true if the char is a capital letter else false
};