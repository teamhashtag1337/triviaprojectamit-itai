#include "DataBase.h"
#include <exception>
#include <cstdio>

#define DBNAME "data.db"

DataBase::DataBase()
{
	int rc;
	stringstream s;
	char* zErrMsg = 0;

	rc = sqlite3_open(DBNAME, &_db);

	if (rc)
	{	
		s << "SQL error: " << sqlite3_errmsg(_db);
		throw exception(s.str().c_str());
	}

	vector<string> commands;
	s.str(string());
	s << "create table if not exists t_users(username text not null, password text not null, email text not null)";
	commands.push_back(s.str());
	s.str(string());
	s << "create table if not exists t_games(game_id integer primary key  autoincrement not null, status integer not null, start_time datetime not null, end_time datetime)";
	commands.push_back(s.str());
	s.str(string());
	s << "create table if not exists t_questions(question_id integer primary key  autoincrement not null, question text not null, correct_ans text not null, ans2 text not null, ans3 text not null, ans4 text not null)";
	commands.push_back(s.str());
	s.str(string());
	s << "create table if not exists t_players_answers(game_id integer not null, username text not null , question_id integer not null, player_answer text not null, is_correct integer not null, answer_time integer not null, foreign key(game_id) references t_games(game_id), foreign key(question_id) references t_questions(question_id), foreign key(username) references t_users(username), primary key(game_id, username, question_id))";
	commands.push_back(s.str());
	s.str(string());
	s << "create table if not exists t_highscores(username text not null primary key, first integer not null, second integer not null, third integer not null, foreign key(username) references t_users(username))";
	commands.push_back(s.str());
	s.str(string());

	for (int i = 0; i < commands.size(); i++)
	{
		rc = sqlite3_exec(_db, commands[i].c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			s << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			throw exception(s.str().c_str());
			
		}

	}


	
}

DataBase::~DataBase()
{
	sqlite3_close(_db);
}

bool DataBase::isUserExisting(string username)
{
	int rc;
	char* zErrMsg;
	bool found = false;
	stringstream s;
	s << "select * from t_users where username=" <<"\"" << username << "\"";
	rc = sqlite3_exec(_db, s.str().c_str(), callbackExist, &found, &zErrMsg);
	return found;
}

bool DataBase::addNewUser(string username, string password, string email) 
{
	try
	{
		
		int rc;
		char* zErrMsg;
		stringstream s;
		vector<string> cmds;

		s << "insert into t_users(username, password, email) values(" << "\"" << username << "\""  << ", " << "\"" << Cryptor::hashStr(password) << "\""  << ", " << "\"" << email << "\"" << ")";
		cmds.push_back(s.str());
		s.str(string());
		s << "insert into t_highscores(username, first, second, third) values(" << "\"" << username << "\"" << ", 0, 0, 0)";
		cmds.push_back(s.str());
		s.str(string());

		for (int i = 0; i < cmds.size(); i++)
		{
			rc = sqlite3_exec(_db, cmds[i].c_str(), NULL, 0, &zErrMsg);
			if (rc != SQLITE_OK)
			{
				return false;
			}
		}
		

		return true;
	}

	catch (exception& e)
	{
		return false;
	}
	
}

bool DataBase::isUserAndPassMatch(string username, string password)
{
	string pass;
	int rc;
	char* zErrMsg;
	stringstream s;
	s << "select * from t_users where username=" << "\""<< username << "\"";
	rc = sqlite3_exec(_db, s.str().c_str(), callbackGetUserPass, &pass, &zErrMsg);
	if (rc != SQLITE_OK || pass != Cryptor::hashStr(password))
	{
		return false;
	}

	return true;
}

vector<Question*> DataBase::initQuestions(int questionsNum)
{
	vector<Question*> questions;
	int rc;
	char* zErrMsg;
	stringstream s;
	s << "SELECT * FROM t_questions order by random() limit " << questionsNum;
	rc = sqlite3_exec(_db, s.str().c_str(), callbackQuestions, &questions, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		return vector<Question*>();
	}

	return questions;
}

vector<string> DataBase::getBestScores()
{
	vector<string> rtn;
	int rc;
	char* zErrMsg;
	stringstream s;
	s << "select * from t_highscores order by first DESC limit 3";
	rc = sqlite3_exec(_db, s.str().c_str(), callbackgetUser, &rtn, &zErrMsg);

	return rtn;
}

vector<string> DataBase::getPersonalStatus(string username)
{
	int rc;
	int id;
	char* zErrMsg;
	string correct;
	string wrong;
	double avgTime;
	string sAvgTime;
	stringstream s;
	vector<string> rtn;
	int count = 0;

	s << "select count(game_id) from t_players_answers where username=\"" << username << "\"";
	rc = sqlite3_exec(_db, s.str().c_str(), callbackCount, &count, &zErrMsg);

	if (rc != SQLITE_OK || count == 0)
	{
		return vector<string>();
	}

	rtn.push_back(Stdafx::parseIntTo4Bytes(count));
	s.str(string());
	count = 0;
	s << "select count(*) from t_players_answers where username=\"" << username << "\" And is_correct=1";
	rc = sqlite3_exec(_db, s.str().c_str(), callbackCount, &count, &zErrMsg);
	
	if (rc != SQLITE_OK)
	{
		return vector<string>();
	}

	s.str(string());
	correct = Stdafx::parseIntToXBytes(count, 6);
	count = 0;

	s << "select count(*) from t_players_answers where username=\"" << username << "\" And is_correct=0";
	rc = sqlite3_exec(_db, s.str().c_str(), callbackCount, &count, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		return vector<string>();
	}

	s.str(string());
	wrong = Stdafx::parseIntToXBytes(count, 6);

	s.str(string());
	s << "select avg(answer_time) from t_players_answers where username=\"" << username << "\"";

	rc = sqlite3_exec(_db, s.str().c_str(), callbackAverage, &avgTime, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		return vector<string>();
	}

	avgTime *= 100;
	avgTime = ((int) avgTime) % 10000; // Making sure it`s a 4 digit number
	s.str(string());
	
	sAvgTime = Stdafx::parseIntTo4Bytes((int)avgTime);

	rtn.push_back(correct);
	rtn.push_back(wrong);
	rtn.push_back(sAvgTime);

	return rtn;
}

int DataBase::insertNewGame()
{
	int rc;
	int id;
	char* zErrMsg;
	stringstream s;
	s << "insert into t_games(status, start_time) values(0,datetime('now'))";
	rc = sqlite3_exec(_db, s.str().c_str(), NULL, 0, &zErrMsg);
	
	if (rc != SQLITE_OK)
	{
		return -1;
	}

	s.str(string());
	s << "select last_insert_rowid()";
	rc = sqlite3_exec(_db, s.str().c_str(), callbackLastId, &id, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		return -1;
	}

	return id;
}

bool DataBase::updateGameStatus(int gameid)
{
	int rc;
	int id;
	char* zErrMsg;
	stringstream s;
	s << "update t_games set status=1, end_time=datetime('now') where game_id=" << gameid;
	rc = sqlite3_exec(_db, s.str().c_str(), NULL, 0, &zErrMsg);

	return (rc == SQLITE_OK);


}

bool DataBase::addAnswerToPlayer(int game_id, string username, int question_id, string player_answer, bool is_correct, int answer_time)
{
	int rc;
	int isCorrect = (is_correct) ? 1 : 0;
	char* zErrMsg;
	stringstream s;
	s << "insert into t_players_answers(game_id, username, question_id, player_answer, is_correct, answer_time) values(" << game_id << ", \"" << username << "\", " << question_id  << ", \"" << player_answer << "\", " << isCorrect << ", " << answer_time << ")";
	rc = sqlite3_exec(_db, s.str().c_str(), NULL, 0, &zErrMsg);
	return (rc == SQLITE_OK);
}

int DataBase::callbackCount(void* notUsed, int argc, char** argv, char** azCol)
{
	int* tmp = (int*)notUsed;
	*tmp = atoi(argv[0]);
	return 0;
}


int DataBase::callbackAverage(void* notUsed, int argc, char** argv, char** azCol)
{
	double* tmp = (double*)notUsed;
	for (int i = 0; i < argc; i++)
	{
		char* colName = azCol[i];
		if (strcmp(colName, "avg(answer_time)") == 0)
		{
			string tmpStr = argv[i];
			double tmpVal = stod(tmpStr, NULL);
			*tmp = tmpVal;
		}
	}
	return 0;
}


int DataBase::callbackQuestions(void* notUsed, int argc, char** argv, char** azCol)
{
	vector<Question*>* vec = (vector<Question*>*) notUsed;
	int question_id;
	string question;
	string correct_ans;
	string ans2, ans3, ans4;

	for (int i = 0; i < argc; i++)
	{
		char* colName = azCol[i];
		if (strcmp(colName, "question_id") == 0)
		{
			question_id = atoi(argv[i]);
		}

		else if (strcmp(colName, "question") == 0)
		{
			question = argv[i];
		}

		else if (strcmp(colName, "correct_ans") == 0)
		{
			correct_ans = argv[i];
		}

		else if (strcmp(colName, "ans2") == 0)
		{
			ans2 = argv[i];
		}

		else if (strcmp(colName, "ans3") == 0)
		{
			ans3 = argv[i];
		}

		else if (strcmp(colName, "ans4") == 0)
		{
			ans4 = argv[i];
		}
	}

	Question* q = new Question(question_id, question, correct_ans, ans2, ans3, ans4);
	vec->push_back(q);
	return 0;
}


int DataBase::callbackHighScores(void* notUsed, int argc, char** argv, char** azCol)
{
	vector<int>* tmp = (vector<int>*) notUsed;
	int score = 0;

	for (int i = 0; i < argc; i++)
	{
		char* colName = azCol[i];
		if (!strcmp(colName, "first") || !strcmp(colName, "second") || !strcmp(colName, "third"))
		{
			score = atoi(argv[i]);
			tmp->push_back(score);	
		}

	}

	return 0;
}

int DataBase::callbackBestScores(void* notUsed, int argc, char** argv, char** azCol)
{

	vector<string>* tmp = (vector<string>*) notUsed;

	for (int i = 0; i < argc; i++)
	{
		char* colName = azCol[i];
		if (!strcmp(colName, "first") || !strcmp(colName, "second") || !strcmp(colName, "third"))
		{
			tmp->push_back(argv[i]);
		}

	}
	return 0;
}

int DataBase::callbackPersonalStatus(void* notUsed, int argc, char** argv, char** azCol)
{
	return 0;
}

int DataBase::callbackGetUserPass(void* notUsed, int argc, char** argv, char** azCol)
{
	string* str = (string*)notUsed;
	for (int i = 0; i < argc; i++)
	{
		char* colName = azCol[i];
		if (strcmp(colName, "password") == 0)
		{
			*str = argv[i];
		}
	}
	return 0;
}


int DataBase::callbackExist(void* notUsed, int argc, char** argv, char** azCol)
{
	bool* ans = (bool*)notUsed;
	*ans = true;
	return 0;
}

int DataBase::callbackLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	int* ans = (int*)notUsed;
	*ans = atoi(argv[0]);
	return 0;
}

int DataBase::callbackgetUser(void* notUsed, int argc, char** argv, char** azCol)
{
	vector<string>* tmp = (vector<string>*) notUsed;

	for (int i = 0; i < argc; i++)
	{
		char* colName = azCol[i];
		if (strcmp(colName, "username") == 0)
		{
			tmp->push_back(argv[i]);
		}
	}
	return 0;

}

int DataBase::callbackgetUserScore(void* notUsed, int argc, char** argv, char** azCol)
{
	vector<int>* tmp = (vector<int>*) notUsed;

	for (int i = 0; i < argc; i++)
	{
		char* colName = azCol[i];
		if (strcmp(colName, "first") == 0)
		{
			tmp->push_back(atoi(argv[i]));
		}
	}
	return 0;

}

vector<int> DataBase::updateHighScore(string username, int score)
{
	vector<int> scores;
	int rc;
	char* zErrMsg;
	stringstream s;
	s << "select * from t_highscores where username=\"" << username << "\"";
	rc = sqlite3_exec(_db, s.str().c_str(), callbackHighScores, &scores, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		return scores;
	}

	vector<int> rtn;

	for (int i = 0; i < scores.size(); i++)
	{
		if (score > scores[i])
		{
			rtn.push_back(score);
			while (rtn.size() < 3)
			{
				rtn.push_back(scores[i++]);
			}
			break;
		}

		else
		{
			rtn.push_back(scores[i]);
		}
	}

	s.str(string());
	s << "update t_highscores set first=" << rtn[0] << ", second=" << rtn[1] << ", third=" << rtn[2] << " where username=\"" << username << "\"";
	rc = sqlite3_exec(_db, s.str().c_str(), NULL, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		return scores;
	}

	return rtn;


}


vector<int> DataBase::getHighScores()
{
	vector<int> scores;
	int rc;
	char* zErrMsg;
	stringstream s;
	s << "select * from t_highscores order by first desc limit 3";
	rc = sqlite3_exec(_db, s.str().c_str(), callbackgetUserScore, &scores, &zErrMsg);

	return scores;



}

