#pragma once

#include "stdafx.h"
#include "Question.h"
#include <vector>
#include <map>
#include "sqlite3.h"
#include <sstream>
#include "Cryptor.h"


class DataBase
{
public:
	DataBase();
	~DataBase();
	bool isUserExisting(string username);
	bool addNewUser(string, string, string);
	bool isUserAndPassMatch(string, string);
	vector<Question*> initQuestions(int);
	vector<string> getBestScores();
	vector<string> getPersonalStatus(string);
	int insertNewGame();
	bool updateGameStatus(int);
	bool addAnswerToPlayer(int, string, int, string, bool, int);
	vector<int> updateHighScore(string username, int score); // Updates the 3 highest scores with the newest score. Returns the new scores in a vector.
	vector<int> getHighScores(); //Returns the top 3 scores.

	sqlite3* _db;
	

private:
	static int callbackCount(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackQuestions(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackBestScores(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackPersonalStatus(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackGetUserPass(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackExist(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackLastId(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackHighScores(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackgetUser(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackgetUserScore(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackAverage(void* notUsed, int argc, char** argv, char** azCol);
};