#pragma once

#include "stdafx.h"
#include "User.h"
#include <vector>
#include <map>
#include "Question.h"
#include "DataBase.h"


using namespace std;

class User;

class Game
{
private:
	vector<Question*> _questions;
	vector<User*> _players;
	int _questionsNo;
	int _currQuestionIndex;
	DataBase* _db;
	map<string, int> _results;
	int _currentTurnAnswers;
	int _gameId;
	int _gameStatus; //Holds 0 if the game is active else 1

	void sendQuestionToAllUsers(); //Sends the current question to all users

public:
	Game(const vector<User*>& questions, int questionsNo, DataBase* db); //Ctor
	~Game(); //Dtor
	void sendFirstQuestion(); //Sends the first question to all users
	void handleFinishGame(); //Closes the game after finish
	bool handleNextTurn(); //Starts the next turn. The game is closed if ended
	bool handleAnswerFromUser(User* user, int answerNo, int time); //Checks the user's answer and updates the database accordingly. Sends a message to the user about the correctence of his answer
	bool leaveGame(User* user); //Removing a user from the game
	int getID(); //Returns _gameId
	int getGameStatus(); //Returns _gameStatus
};