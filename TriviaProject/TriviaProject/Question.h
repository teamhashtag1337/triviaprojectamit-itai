#pragma once

#include "stdafx.h"
#include <ctime>

class Question
{
private:
	string _question;
	string _answers[4];
	int _correctAnswerIndex;
	int _id;

public:
	Question(int id, string question, string answer1, string answer2, string answer3, string answer4); //Ctor. Answer 1 is the right answer
	string getQuestion(); //Returns _question
	string* getAnswers(); //Returns _answers
	int getCorrectAnswerIndex(); //Returns _correctAnswerIndex
	int getId(); //Returns _id
};