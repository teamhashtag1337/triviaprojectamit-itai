#pragma once

#include "stdafx.h"
#include "Room.h"
#include "Game.h"
#include "Cryptor.h"

class Room;
class Game;

class User
{
private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _sock;
	Cryptor* _cryptor;


public:
	User(string name, SOCKET sock); //Ctor
	void send(string message); //Sends a message to the user
	string getUserName(); //Returns _username
	SOCKET getSocket(); //Returns _sock
	void setCryptor(Cryptor* cryptor);
	void setGame(Game* gm); //Sets _currGame to gm
	Room* getRoom(); // Returns _currRoom
	Game* getGame(); //Returns _currGame
	void clearGame(); //Sets _currGame to nullptr
	bool createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime); //Creates a new room and sets _currRoom to it
	bool joinRoom(Room* newRoom); //Adds a player to a room
	void leaveRoom(); //Leaves the room the user is in. Sets _currRoom to nullptr
	int closeRoom(); //Closes the room the user is in. Sets _currRoom to nullptr
	bool leaveGame(); //Leaves the game that the user is in. Sets _currGame to nullptr
	void clearRoom(); //Sets _currRoom to nullptr
	bool operator==(User* other); //Returns true if usernames of the current and other user are equal
	void setRoom(Room* room);
};