#pragma once

#include <cstdio>
#include <iostream>
#include "osrng.h"
#include "modes.h"
#include <string.h>
#include <cstdlib>
#include <sha.h>
#include <hex.h>
#include <filters.h>
#include <vector>
#include <rsa.h>
#include <integer.h>


using namespace std;


class Cryptor
{

public:
	static const int KEY_LENGTH_BYTES = 384;
	static string hashStr(string input);
	char* encryptRSA(string input);
	string decryptRSA(char* input);
	void readPublicKey(string key);
	string encodePublicKey();
	void setFriendModulus(char* key);
	void setFriendExponent(char* exp);
	char* getExponent();
	char* getModulus();
	void initalize(CryptoPP::Integer n, CryptoPP::Integer e);
	int getExponentLength();
	int getModulusLength();
	Cryptor();
	~Cryptor();
private:
	static CryptoPP::SHA256 _hash;
	CryptoPP::RSA::PrivateKey* _privateKey;
	CryptoPP::RSA::PublicKey* _publicKey;
	CryptoPP::RSA::PublicKey* _friendPublicKey;
	CryptoPP::AutoSeededRandomPool _rng;

};
