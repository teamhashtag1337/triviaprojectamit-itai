#include "Validator.h"

bool Validator::isPasswordValid(string password)
{
	bool smallLetterFlag = false;
	bool capitalLetterFlag = false;
	bool numberFlag = false;
	if (password.length() >= 4 && find(password.begin(), password.end(), ' ') == password.end()) //If the password's length is at least 4 and the password contains no spaces
	{
		for (Uint counter = 0; counter < password.length(); counter++) //For each letter in the password
		{
			if (!smallLetterFlag && isSmallLetter(password[counter])) //Checking for small letters, capital letters and numbers
				smallLetterFlag = true;
			if (!capitalLetterFlag && isCapitalLetter(password[counter]))
				capitalLetterFlag = true;
			if (!numberFlag && isNumber(password[counter]))
				numberFlag = true;
		}
		return (smallLetterFlag && capitalLetterFlag && numberFlag); //If the password contains at least one small letter, one capital letter and one number return true else false
	}
	else return false;
}

bool Validator::isUsernameValid(string username)
{
	return(!username.empty() && isLetter(username[0]) && find(username.begin(), username.end(), ' ') == username.end()); //If the username is not empty, the username starts with a letter and the username contains no spaces return true else false
}

bool Validator::isLetter(char c)
{
	return (isSmallLetter(c) || isCapitalLetter(c));
}

bool Validator::isNumber(char c)
{
	return (c >= '0' && c <= '9');
}

bool Validator::isSmallLetter(char c)
{
	return (c >= 'a' && c <= 'z');
}

bool Validator::isCapitalLetter(char c)
{
	return (c >= 'A' && c <= 'Z');
}