#include "Game.h"

#define ANS_ARR_LENGTH 4
#define TIME_OUT_NO 5


Game::Game(const vector<User*>& players, int questionsNo, DataBase* db) 
{
	_db = db;
	_gameStatus = ACTIVE;
	_gameId = db->insertNewGame(); //Inserting a new game to the database
	_questions = _db->initQuestions(questionsNo); //Initiating the questions
	if (_questions.size() < questionsNo)
	{
		throw exception("Not enough questions in the database.");
	}
	_players = players;
	_questionsNo = questionsNo;
	for (auto iter = players.begin(); iter != players.end(); iter++) //Setting each user's game to this game
	{
		(*iter)->setGame(this);
	}
	_currQuestionIndex = 0;
}

Game::~Game()
{
	for (auto iter = _questions.begin(); iter != _questions.end(); iter++) //Deleting each question in the vector
	{
		delete *iter;
	}
	for (auto iter = _players.begin(); iter != _players.end(); iter++) //Deleting each user in the vector
	{
		delete *iter;
	}
}

void Game::sendQuestionToAllUsers()
{
	string message = "118";
	message += Stdafx::parseIntToXBytes(_questions[_currQuestionIndex]->getQuestion().length(), 3); //Adding the question's length to the string
	message += _questions[_currQuestionIndex]->getQuestion(); //Adding the question to the string
	string* tmp = _questions[_currQuestionIndex]->getAnswers();
	for (int counter = 0; counter < ANS_ARR_LENGTH; counter++) //Adding each answer length and data to the string
	{
		message += Stdafx::parseIntToXBytes(tmp[counter].length(), 3);
		message += tmp[counter];
	}
	_currentTurnAnswers = 0;
	for (auto iter = _players.begin(); iter != _players.end(); iter++) //Sending a 118 message to each player
	{
		try
		{
			(*iter)->send(message);
		}
		catch (exception& e)
		{

		}
	}

	cout << "Message: " << message << endl;
}

void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}

void Game::handleFinishGame()
{
	string message = "121";
	_db->updateGameStatus(_gameId);
	message += ((_players.size()) % 10 + '0'); //Adding the number of players to the message
	for (auto iter = _players.begin(); iter != _players.end(); iter++) //Preparing the 121 message
	{		
		message += Stdafx::parseInt((*iter)->getUserName().length()); //Adding the length of the username to the message
		message += (*iter)->getUserName(); //Adding the username to the message
		message += Stdafx::parseInt(_results[(*iter)->getUserName()]); //Adding the user's score to the messages
	}
	for (auto iter = _players.begin(); iter != _players.end(); iter++) //Closing the game for each connected player
	{
		try
		{
			_db->updateHighScore((*iter)->getUserName(), _results[(*iter)->getUserName()]);
			(*iter)->send(message); //Sending 121 message to each user
		}
		catch (exception& e)
		{

		}
		(*iter)->clearGame();
	}
	cout << "Finished game. " << endl;
	_gameStatus = INACTIVE;
}

bool Game::handleNextTurn()
{
	if (_gameStatus == INACTIVE) //If the game is inactive
		return false;

	if (_players.size() == 0) //If there are no more players in the game
		handleFinishGame();
	else
	{
		if (_players.size() == _currentTurnAnswers) //If all the players answered the question
		{
			if (_currQuestionIndex == _questions.size() - 1) //If its the last qustion
				handleFinishGame();
			else
			{
				_currQuestionIndex++;
				sendQuestionToAllUsers(); //Sending the next question to all users
			}
		}
	}
	return true;
}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	bool correct = false;
	if (_gameStatus == INACTIVE) //If the game is inactive
		return false;

	_currentTurnAnswers++;
	if (correct = (_questions[_currQuestionIndex]->getCorrectAnswerIndex() == answerNo)) //If the user answered the question correctly
		_results[user->getUserName()]++; //Adding 1 to the score of the player
	if (answerNo == TIME_OUT_NO) //If the player did not answer in time
		_db->addAnswerToPlayer(_gameId, user->getUserName(), _questions[_currQuestionIndex]->getId(), "", false, time);
	else
		_db->addAnswerToPlayer(_gameId, user->getUserName(), _questions[_currQuestionIndex]->getId(), _questions[_currQuestionIndex]->getAnswers()[answerNo - 1], correct, time);
	string message = "120";
	message += (correct) ? "1" : "0"; //Adding 1 if the answer is correct else 0
	user->send(message); //Sending a 120 message to the user
	handleNextTurn();
	return true;
}

bool Game::leaveGame(User* user)
{
	if (_gameStatus == INACTIVE) //If the game is inactive
		return false;

	for (auto iter = _players.begin(); iter != _players.end(); iter++) //If the user is in the users list
	{
		if (*iter != NULL && *iter == user)
		{
			(*iter)->setGame(nullptr);
			_players.erase(iter); //Removing the user from the list
			handleNextTurn();
			break;
		}
	}
	return true;
}

int Game::getID()
{
	return _gameId;
}

int Game::getGameStatus()
{
	return _gameStatus;
}
