
#include "TriviaServer.h"
#include <sstream>


using namespace std;
mutex messages_mutex;
condition_variable cond;
condition_variable stopcond;
int TriviaServer::_roomIdSequence = 0;

TriviaServer::TriviaServer() : _currentUser(NULL)
{
	// notice that we step out to the global namespace
	// for the resolution of the function socket

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
	_db = new DataBase();

	
}

TriviaServer::~TriviaServer()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		auto it = _connectedUsers.begin();
		for (; it != _connectedUsers.end(); it++)
		{
			delete it->second;
		}

		auto itb = _connections.begin();
		for (; itb != _connections.end(); itb++)
		{
			delete itb->second;
		}

		while (!_messages.empty())
		{
			delete _messages.front();
			_messages.pop();
		}
		::closesocket(_serverSocket);

		delete _db;
	}
	catch (...) {}
}

void TriviaServer::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (::listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	cout << "Listening on port " << port << endl;

	thread updateThead(&TriviaServer::handleRecievedMessages, this);
	updateThead.detach();
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		cout << "Waiting for client connection request" << endl;
		accept();
	}
}


void TriviaServer::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	cout << "Client accepted. Server and client can speak" << endl;

	// the function that handle the conversation with the client
	//Creating the threads here
	std::thread handleClientThread(&TriviaServer::clientHandler, this, client_socket);
	handleClientThread.detach();
}


void TriviaServer::sendKeyToClient(SOCKET clientSocket)
{
	string message = "401"; // Server encryption request
	char* exp = _cryptor.getExponent();
	int modlength = _cryptor.getModulusLength();
	char* mod = _cryptor.getModulus();
	string len = Stdafx::parseIntTo4Bytes(_cryptor.getExponentLength());

	message += len;
	Helper::sendData(clientSocket, message);
	Helper::sendData(clientSocket, exp, _cryptor.getExponentLength());
	Helper::sendData(clientSocket, mod, modlength);
	delete exp;
	delete mod;


}

void TriviaServer::clientHandler(SOCKET clientSocket)
{
	try
	{

		stringstream sstream;
		sendKeyToClient(clientSocket);
		int msgCode = Helper::getMessageTypeCode(clientSocket);
		buildRecievedMessage(clientSocket, msgCode);

		waitForAuth();

		while (msgCode != 0 && msgCode != Protocol::USR_EXIT)
		{	
			
			char* encryptedMessage = Helper::getPartFromSocket(clientSocket, Cryptor::KEY_LENGTH_BYTES);
			string decryptedMessage = _cryptor.decryptRSA(encryptedMessage);
			sstream << decryptedMessage;
			msgCode = Helper::getMessageTypeCode((stringstream&)sstream);
			buildRecievedMessage(clientSocket, msgCode, sstream);
			sstream.str(string());
		}

		buildRecievedMessage(clientSocket, Protocol::USR_EXIT, sstream);
		closesocket(clientSocket);
	}
	catch (const std::exception& e)
	{
		
		cout << "Socket " << clientSocket << " was closed because: " << e.what() << endl;
		closesocket(clientSocket);
	}





}


void TriviaServer::waitForAuth()
{
	mutex waiter;
	unique_lock<mutex> guard(waiter);
	stopcond.wait(guard);
}

void TriviaServer::handleRecievedMessages()
{
	while (true)
	{
		unique_lock<mutex> guard(messages_mutex);
		User* user;
		cond.wait(guard, [&](){return !_messages.empty(); });

		while (!_messages.empty())
		{
			RecievedMessage* message = _messages.front();
			switch (message->getMessageCode())
			{
			case Protocol::USR_LOG_IN:
				cout << "---------------\n\nUser is trying to sign in.\n\n------\n\n";
				user = handleSignin(message);
				if (user != NULL)
				{
					_connectedUsers.insert(pair<SOCKET, User*>(message->getSocket(), user));
					user->setCryptor(getCryptorBySocket(message->getSocket()));
				}
				break;
			case Protocol::USR_LOG_OUT:
			case Protocol::USR_EXIT:
				cout << "User has logged out or exited.. Deleting user data.\n\n";
				handleCloseRoom(message);
				handleLeaveRoom(message);
				safeDeleteUser(message);
				if (message->getMessageCode() == Protocol::USR_EXIT)
					safeDeleteConnection(message);
				break;
			case Protocol::USR_SIGN_UP:
				cout << "---------------\n\nUser is trying to sign up.\n\n------\n\n";
				if (handleSignup(message))
				{
					Helper::sendData(message->getSocket(), "1040", getCryptorBySocket(message->getSocket())); //Success
				}
				break;
			case Protocol::USR_ROOM_LIST:
				handleGetRooms(message);
				break;
			case Protocol::USR_ROOM_USRLIST:
				handleGetUsersInRoom(message);
				break;
			case Protocol::USR_ROOM_CREATE:
				if (handleCreateRoom(message))
				{
					Helper::sendData(message->getSocket(), "1140", getCryptorBySocket(message->getSocket())); //Success
					cout << "-----------\n\n Room created successfully\n\n-----------\n\n";
					
				}
				break;
			case Protocol::USR_ROOM_CLOSE:
				if (handleCloseRoom(message))
				{
					cout << "-----------\n\n Room closes successfully\n\n --------------\n\n";
				}
				break;
			case Protocol::USR_ROOM_JOIN:
				if (handleJoinRoom(message))
				{
					cout << "----------\n\n The player has joined a room successfully.\n\n ----------------\n\n";
					
					User* user = getUserBySocket(message->getSocket());
					string msg = buildMessage(1, "", user->getUserName());
					user->getRoom()->sendMessage(user, msg);
					
				}
				break;
			case Protocol::USR_ROOM_LEAVE:
				if (handleLeaveRoom(message))
				{
					cout << "----------\n\n The player has left a room successfully.\n\n ----------------\n\n";
				}
				break;
			case Protocol::USR_GAME_START:
				handleStartGame(message);
				cout << "----------\n\n A game has started.\n\n ----------------\n\n";
				break;
			case Protocol::USR_GAME_PLAYER_ANS:
				handlePlayerAnswer(message);
				cout << "----------\n\n Handled a player answer request.\n\n ----------------\n\n";
				break;
			case Protocol::USR_GAME_LEAVE:
				handleLeaveGame(message);
				break;
			case Protocol::USR_BEST_SCORES:
				handleBestScores(message);
				break;
			case Protocol::USR_PERSONAL_STATUS:
				handleGetPersonalStatus(message);
				break;
			case Protocol::USR_CHATROOM_MSG:
				handleChatMessage(message);
				break;
			case Protocol::USR_ENCRYPTION_RES:
				handleKeyRecieve(message);
				break;
			default:
				break;
			}
			_messages.pop();
		}

		

	}
}


bool TriviaServer::handleSignup(RecievedMessage* message) //Add email implementation after db is used
{
	
	string username = message->getValues()[0];
	string password = message->getValues()[1];
	string email = message->getValues()[2];

	if (!Validator::isUsernameValid(username))
	{
		Helper::sendData(message->getSocket(), "1043", getCryptorBySocket(message->getSocket())); //Ilegal username
		return false;
	}

	if (!Validator::isPasswordValid(password))
	{
		Helper::sendData(message->getSocket(), "1041", getCryptorBySocket(message->getSocket())); //Ilegal password
		return false;
	}

	if (_db->isUserExisting(username))
	{
		Helper::sendData(message->getSocket(), "1042", getCryptorBySocket(message->getSocket()));
		return false;
	}

	if (!_db->addNewUser(username, password, email)) //Email doesn`t currently have any meaning until database update.
	{
		Helper::sendData(message->getSocket(), "1044", getCryptorBySocket(message->getSocket())); //Other
		return false;
	}

	return true;
}

void TriviaServer::addRecievedMessage(RecievedMessage* message)
{
	_messages.push(message);
	cond.notify_all();
}

void TriviaServer::buildRecievedMessage(SOCKET socket, int msgcode, stringstream& message)
{
	vector<string> vals;
	int firstLength;
	int secondLength;
	int thirdLength;

	switch (msgcode)
	{
	case Protocol::USR_LOG_IN:
		firstLength = Helper::getIntPartFromSocket(message, 2);
		vals.push_back(Helper::getStringPartFromSocket(message, firstLength)); //username
		secondLength = Helper::getIntPartFromSocket(message, 2);
		vals.push_back(Helper::getStringPartFromSocket(message, secondLength)); //pass
		break;
	case Protocol::USR_SIGN_UP:
		firstLength = Helper::getIntPartFromSocket(message, 2);
		vals.push_back(Helper::getStringPartFromSocket(message, firstLength)); //username
		secondLength = Helper::getIntPartFromSocket(message, 2);
		vals.push_back(Helper::getStringPartFromSocket(message, secondLength)); //pass
		thirdLength = Helper::getIntPartFromSocket(message, 2);
		vals.push_back(Helper::getStringPartFromSocket(message, thirdLength)); //email
		break;
	case Protocol::USR_ROOM_CREATE:
		firstLength = Helper::getIntPartFromSocket(message, 2);
		vals.push_back(Helper::getStringPartFromSocket(message, firstLength)); //roomname
		vals.push_back(Helper::getStringPartFromSocket(message, 1)); //numofplayers
		vals.push_back(Helper::getStringPartFromSocket(message, 2)); //numofquestions
		vals.push_back(Helper::getStringPartFromSocket(message, 2)); //questiontime
		break;
	case Protocol::USR_ROOM_USRLIST: //both need to only push roomid
	case Protocol::USR_ROOM_JOIN:
		vals.push_back(Helper::getStringPartFromSocket(message, 4)); //roomid
		break;
	case Protocol::USR_CHATROOM_MSG:
		secondLength = Helper::getIntPartFromSocket(message, 2); //Username length
		vals.push_back(Helper::getStringPartFromSocket(message, secondLength)); //Username
		firstLength = Helper::getIntPartFromSocket(message, 4); // Message length
		vals.push_back(Helper::getStringPartFromSocket(message, firstLength)); //Message	
		break;
	case Protocol::USR_LOG_OUT:
	case Protocol::USR_EXIT:
	case Protocol::USR_ROOM_LIST:
	case Protocol::USR_ROOM_CLOSE:
	case Protocol::USR_ROOM_LEAVE:
	case Protocol::USR_GAME_START:
	case Protocol::USR_BEST_SCORES:
	case Protocol::USR_PERSONAL_STATUS:
	case Protocol::USR_ENCRYPTION_RES:
	case Protocol::USR_GAME_LEAVE:
		break;
	case Protocol::USR_GAME_PLAYER_ANS:
		vals.push_back(Helper::getStringPartFromSocket(message, 1)); //answerNumber
		vals.push_back(Helper::getStringPartFromSocket(message, 2)); //timeInSeconds
		break;
	default:
		cout << "\nGot an unsupported message: " << msgcode << endl << endl;
		break;
	}

	RecievedMessage* msg = new RecievedMessage(socket, msgcode, vals);
	addRecievedMessage(msg);
}

void TriviaServer::buildRecievedMessage(SOCKET socket, int msgcode)
{
	vector<string> vals;
	int firstLength;
	int secondLength;
	int thirdLength;

	switch (msgcode)
	{
	case Protocol::USR_LOG_IN:
		firstLength = Helper::getIntPartFromSocket(socket, 2);
		vals.push_back(Helper::getStringPartFromSocket(socket, firstLength)); //username
		secondLength = Helper::getIntPartFromSocket(socket, 2);
		vals.push_back(Helper::getStringPartFromSocket(socket, secondLength)); //pass
		break;
	case Protocol::USR_SIGN_UP:
		firstLength = Helper::getIntPartFromSocket(socket, 2);
		vals.push_back(Helper::getStringPartFromSocket(socket, firstLength)); //username
		secondLength = Helper::getIntPartFromSocket(socket, 2);
		vals.push_back(Helper::getStringPartFromSocket(socket, secondLength)); //pass
		thirdLength = Helper::getIntPartFromSocket(socket, 2);
		vals.push_back(Helper::getStringPartFromSocket(socket, thirdLength)); //email
		break;
	case Protocol::USR_ROOM_CREATE:
		firstLength = Helper::getIntPartFromSocket(socket, 2);
		vals.push_back(Helper::getStringPartFromSocket(socket, firstLength)); //roomname
		vals.push_back(Helper::getStringPartFromSocket(socket, 1)); //numofplayers
		vals.push_back(Helper::getStringPartFromSocket(socket, 2)); //numofquestions
		vals.push_back(Helper::getStringPartFromSocket(socket, 2)); //questiontime
		break;
	case Protocol::USR_ROOM_USRLIST: //both need to only push roomid
	case Protocol::USR_ROOM_JOIN:
		vals.push_back(Helper::getStringPartFromSocket(socket, 4)); //roomid
		break;
	case Protocol::USR_CHATROOM_MSG:
		secondLength = Helper::getIntPartFromSocket(socket, 2); //Username length
		vals.push_back(Helper::getStringPartFromSocket(socket, secondLength)); //Username
		firstLength = Helper::getIntPartFromSocket(socket, 4); // Message length
		vals.push_back(Helper::getStringPartFromSocket(socket, firstLength)); //Message	
			break;
	case Protocol::USR_LOG_OUT:
	case Protocol::USR_EXIT:
	case Protocol::USR_ROOM_LIST:
	case Protocol::USR_ROOM_CLOSE:
	case Protocol::USR_ROOM_LEAVE:
	case Protocol::USR_GAME_START:
	case Protocol::USR_BEST_SCORES:
	case Protocol::USR_PERSONAL_STATUS:
		break;
	case Protocol::USR_GAME_PLAYER_ANS:
		vals.push_back(Helper::getStringPartFromSocket(socket, 1)); //answerNumber
		vals.push_back(Helper::getStringPartFromSocket(socket, 2)); //timeInSeconds
		break;
	case Protocol::USR_ENCRYPTION_RES:
		vals.push_back(Helper::getStringPartFromSocket(socket, 4)); //Exponent length
		break;
	default:
		cout << "\nGot an unsupported message: " << msgcode << endl << endl;
		break;
	}

	if (msgcode == Protocol::USR_ENCRYPTION_RES)
	{
		RecievedMessage* message = new RecievedMessage(socket, msgcode, vals);
		addRecievedMessage(message);
	}

	else
	{
		cout << "Someone tried to send a plain text message that didn`t fit the protocol! :O Luckily, we took care of the socket as well. They cannot run away from it, now can they?" << endl << endl;
	}
	
}


void TriviaServer::handleKeyRecieve(RecievedMessage* message)
{
	string key = message->getValues()[0];
	Cryptor* newCryptor = new Cryptor();

	//Reading Mod and Exp here
	int len = stoi(key);
	CryptoPP::Integer e = Helper::getExponentPartFromSocket(message->getSocket(), len);
	CryptoPP::Integer n = Helper::getModulusPartFromSocket(message->getSocket());
	newCryptor->initalize(n, e);

	_connections.insert(pair<SOCKET, Cryptor*>(message->getSocket(), newCryptor));
	cout << "Key recieved.";
	stopcond.notify_one();
}

Cryptor* TriviaServer::getCryptorByUser(User* user)
{
	auto it = _connections.find(user->getSocket());

	if (it == _connections.end());
	return NULL;

	return it->second;
}

User* TriviaServer::handleSignin(RecievedMessage* message)
{
	string username = message->getValues()[0]; //First argument is the username
	string password = message->getValues()[1]; //Second argument is the password

	if (!_db->isUserExisting(username) || !_db->isUserAndPassMatch(username, password))
	{
		Helper::sendData(message->getSocket(), "1021", getCryptorBySocket(message->getSocket())); //Wrong details
		cout << "------\n\nLogin failed.\n\n------\n\n";
		return NULL;
	}

	if (getUserByName(username) != NULL)
	{
		Helper::sendData(message->getSocket(), "1022", getCryptorBySocket(message->getSocket())); //User is already connected
		cout << "------\n\nLogin failed.\n\n------\n\n"; 
		return NULL;
	}

	Helper::sendData(message->getSocket(), "1020", getCryptorBySocket(message->getSocket())); //Login success
	cout << "------\n\nLogin successful.\n\n------\n\n";
	return new User(username, message->getSocket());
		
}

User* TriviaServer::getUserByName(string username)
{
	auto it = _connectedUsers.begin();
	for (; it != _connectedUsers.end(); it++)
	{
		if (it->second->getUserName() == username)
		{
			return it->second;
		}
	}

	return NULL;
}


Cryptor* TriviaServer::getCryptorBySocket(SOCKET socket)
{
	auto it = _connections.find(socket);

	if (it == _connections.end())
	{
		return NULL;
	}

	return it->second;
}

void TriviaServer::handleGetRooms(RecievedMessage* message)
{
	string str = "106";
	str += Stdafx::parseIntTo4Bytes(_roomsList.size());
	
	auto it = _roomsList.begin();
	for (; it != _roomsList.end(); it++)
	{
		str += Stdafx::parseIntTo4Bytes(it->first);
		str += Stdafx::parseInt(it->second->getName().size());
		str += it->second->getName();
	}


	Helper::sendData(message->getSocket(), str, getCryptorBySocket(message->getSocket()));

}

void TriviaServer::handleGetUsersInRoom(RecievedMessage* message)
{
	int id = atoi(((message->getValues())[0]).c_str()); // first argument is the RoomID.
	auto it = _roomsList.find(id);

	if (it == _roomsList.end())
	{
		Helper::sendData(message->getSocket(), "1080", getCryptorBySocket(message->getSocket())); //Empty room
		return;
	}

	string rtn = it->second->getUsersListMessage();
	Helper::sendData(message->getSocket(), rtn, getCryptorBySocket(message->getSocket()));
}


User* TriviaServer::getUserBySocket(SOCKET socket)
{
	auto it = _connectedUsers.find(socket);

	if (it == _connectedUsers.end())
	{
		return NULL;
	}

	return it->second;
}

bool TriviaServer::handleCreateRoom(RecievedMessage* message)
{
	User* user = getUserBySocket(message->getSocket());
	if (user == NULL)
	{
		cout << "-----------\n\n Room creation failed\n\n-----------\n\n";
		Helper::sendData(message->getSocket(), "1141", getCryptorBySocket(message->getSocket())); //Failed
		return false;
	}

	vector<string> vals = message->getValues();
	string roomname = vals[0];
	int playersNum = atoi(vals[1].c_str());
	int questionsNum = atoi(vals[2].c_str());
	int questionsTime = atoi(vals[3].c_str());
	int id = _roomIdSequence++;

	if (user->createRoom(id, roomname, playersNum, questionsNum, questionsTime))
	{
		_roomsList.insert(pair<int, Room*>(id, user->getRoom()));
		return true;
	}

	return false;
	


}

bool TriviaServer::handleCloseRoom(RecievedMessage* message)
{
	User* user = getUserBySocket(message->getSocket());
	if (user == NULL)
	{
		return false;
	}
	Room* room = user->getRoom();
	
	if (room == NULL)
	{
		return false;
	}

	int id = room->getId();
	if (user->closeRoom() != -1)
	{
		_roomsList.erase(id);
		return true;
	}

	return false;
}

Room* TriviaServer::getRoomById(int id)
{
	auto it = _roomsList.find(id);

	if (it == _roomsList.end())
	{
		return NULL;
	}

	return it->second;
}

void TriviaServer::safeDeleteUser(RecievedMessage* message)
{
	User* user = getUserBySocket(message->getSocket());
	delete user;
	_connectedUsers.erase(message->getSocket());
}

void TriviaServer::safeDeleteConnection(RecievedMessage* message)
{
	_connections.erase(message->getSocket());
}
bool TriviaServer::handleJoinRoom(RecievedMessage* message)
{
	User* user = getUserBySocket(message->getSocket());

	if (user == NULL)
	{
		Helper::sendData(message->getSocket(), "1102", getCryptorBySocket(message->getSocket())); //Error finding user
		return false;
	}

	int id = atoi(message->getValues()[0].c_str());
	Room* room = getRoomById(id);
	string msg = "110";

	if (room == NULL)
	{
		Helper::sendData(message->getSocket(), "1101", getCryptorBySocket(message->getSocket())); //Room is full
		return false;
	}

	if (user->joinRoom(room))
	{
		msg += '0';
		msg += Stdafx::parseInt(room->getQuestionsNo());
		msg += Stdafx::parseInt(room->getQuestionsTime());
		Helper::sendData(message->getSocket(), msg, getCryptorBySocket(message->getSocket())); //Success
		return true;
	}

	return false;


}

bool TriviaServer::handleLeaveRoom(RecievedMessage* message)
{
	User* user = getUserBySocket(message->getSocket());
	if (!user)
	{
		return false;
	}

	Room* room = user->getRoom();

	if (!room)
	{
		return false;
	}
	

	user->leaveRoom();
	return true;
}

void TriviaServer::handleStartGame(RecievedMessage* message)
{
	User* user = getUserBySocket(message->getSocket());
	Room* room = user->getRoom();

	try
	{	
		Game* game = new Game(room->getUsers(), room->getQuestionsNo(), _db);
		_roomsList.erase(room->getId());
		game->sendFirstQuestion();
		
	}

	catch (exception& e)
	{
		cout  << "Error: " << e.what() << endl;
		user->send("1180");
	}
}

void TriviaServer::handlePlayerAnswer(RecievedMessage* message)
{
	User* user = getUserBySocket(message->getSocket());
	Game* game = user->getGame();
	int answerNumber;
	int timeInSeconds;
	

	if (game != NULL)
	{
		answerNumber = atoi(message->getValues()[0].c_str());
		timeInSeconds = atoi(message->getValues()[1].c_str());
		if (user->getGame() != NULL && !game->handleAnswerFromUser(user, answerNumber, timeInSeconds))
		{
			delete game;
		}
	}
}

void TriviaServer::handleLeaveGame(RecievedMessage* message)
{
	User* user = getUserBySocket(message->getSocket());
	Game* game = user->getGame();
	if (user->leaveGame() && game->getGameStatus() == INACTIVE)
	{
		cout << "A game was released from the memory." << endl;
		delete game;
	}
}

void TriviaServer::handleBestScores(RecievedMessage* message)
{
	vector<int> scores = _db->getHighScores();
	vector<string> users = _db->getBestScores();
	User* user = getUserBySocket(message->getSocket());
	if (user == NULL)
	{
		cout << "Failed handling highscores." << endl;
		return;
	}

	string s = "124";

	for (int i = 0; i < 3; i++)
	{
		if (i < scores.size())
		{
			s += Stdafx::parseInt(users[i].size());
			s += users[i];
			s += Stdafx::parseIntToXBytes(scores[i], 6);
		}

		else
		{
			s +="0" + Stdafx::parseIntToXBytes(0, 6);

		}
	}

	user->send(s);
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage* message)
{
	User* user = getUserBySocket(message->getSocket());

	if (user == NULL)
	{
		return;
	}

	vector<string> ans = _db->getPersonalStatus(user->getUserName());

	if (ans.size() == 0)
	{
		user->send("1260000"); // Failed
		return;
	}
	string ansMsg = "126";

	for (int i = 0; i < ans.size(); i++)
	{
		ansMsg += ans[i];
	}

	user->send(ansMsg);
}


void TriviaServer::handleChatMessage(RecievedMessage* message)
{
	User* user = getUserBySocket(message->getSocket());
	if (user == NULL)
	{
		return;
	}

	Room* room = user->getRoom();
	if (room == NULL)
	{
		cout << "Someone was apparently trying to send a message, and he isn`t in a room... Weird, isn`t it? \n";
		return;
	}


	vector<string> vals = message->getValues();
	string messageStr = buildMessage(0, vals[1], vals[0]);
	room->sendMessage(messageStr);
}

string TriviaServer::buildMessage(int code, string message, string username)
{
	//Building the message here!
	string messageStr = "301"; //Server chatroom response

	messageStr += Stdafx::parseIntToXBytes(code, 1);
	messageStr += Stdafx::parseIntToXBytes(username.length(), 2);
	messageStr += username;
	messageStr += Stdafx::parseIntTo4Bytes(message.length());
	messageStr += message;
	

	return messageStr;
}