#include "stdafx.h"


string Stdafx::parseInt(int num)
{
	if (num >= 10)
	{
		string parsed = "";
		while (num != 0) //Putting the number in string form (reversed) in tmp
		{
			parsed += ((num % 10) + '0');
			num /= 10;
		}
		reverse(parsed.begin(), parsed.end()); //Reversing the string
		return parsed;
	}
	else
	{
		string parsed = "0";
		parsed += num + '0';
		return parsed;
	}
		
}


string Stdafx::parseIntToXBytes(int number, int bytes)
{
	int currentNumber = number;
	string rtn;

	while (currentNumber != 0)
	{
		rtn += (currentNumber % 10) + '0';
		currentNumber /= 10;
	}

	while (rtn.size() < bytes)
	{
		rtn += '0';
	}

	reverse(rtn.begin(), rtn.end());

	return rtn;

}
string Stdafx::parseIntTo4Bytes(int number)
{
	
	return parseIntToXBytes(number, 4);
}