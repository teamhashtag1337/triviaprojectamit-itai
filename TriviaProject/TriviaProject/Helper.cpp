#include "Helper.h"

#include <string>
#include <iomanip>
#include <sstream>


#define DIGESTSIZE Cryptor::KEY_LENGTH_BYTES;
using namespace std;

// recieves the type code of the message from socket (first byte)
// and returns the code. if no message found in the socket returns 0 (which means the client disconnected)
int Helper::getMessageTypeCode(SOCKET sc)
{
	char* s = getPartFromSocket(sc, 3);
	std::string msg(s);

	if (msg == "")
		return 0;

	int res = std::atoi(s);
	delete s;
	return  res;
}

int Helper::getMessageTypeCode(stringstream& message)
{
	return getIntPartFromSocket(message, 3);
}

int Helper::getMessageTypeCode(SOCKET sc, Cryptor& cryptor)
{
	char* s = getPartFromSocket(sc, 3);
	std::string msg(s);

	if (msg == "")
		return 0;

	string decrypted = cryptor.decryptRSA(s);
	int res = std::stoi(decrypted);
	delete s;
	return  res;
}








// send data to socket
// this is private function
void Helper::sendData(SOCKET sc, std::string message) 
{
	const char* data = message.c_str();
	
	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}

void Helper::sendData(SOCKET sc, char* data, int size)
{
	if (send(sc, data,size, 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}

void Helper::sendData(SOCKET sc, std::string message, Cryptor* cryptor)
{
	char* encrypted = cryptor->encryptRSA(message);
	sendData(sc, encrypted, Cryptor::KEY_LENGTH_BYTES);
	delete encrypted;
}


CryptoPP::Integer Helper::getModulusPartFromSocket(SOCKET sc)
{
	char* mod = getPartFromSocket(sc, Cryptor::KEY_LENGTH_BYTES);
	CryptoPP::Integer rtn;
	rtn.Decode((byte*)mod, Cryptor::KEY_LENGTH_BYTES);
	return rtn;
	
}

CryptoPP::Integer Helper::getExponentPartFromSocket(SOCKET sc, int bytesNum)
{
	char* exp =  getPartFromSocket(sc, bytesNum);
	CryptoPP::Integer rtn;
	rtn.Decode((byte*)exp, bytesNum);
	return rtn;

}
int Helper::getIntPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s= getPartFromSocket(sc, bytesNum, 0);
	return atoi(s);
}

string Helper::getStringPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	string res(s);
	return res;
}

string Helper::getStringPartFromSocket(stringstream& message, int bytesNum)
{
	char* s = new char[bytesNum + 1];
	message.read(s, bytesNum);
	s[bytesNum] = 0;
	string rtn = string(s);
	delete s;
	return rtn;
	
}

int Helper::getIntPartFromSocket(stringstream& message, int bytesNum)
{
	string num = getStringPartFromSocket(message, bytesNum);

	return stoi(num);
}


string Helper::getStringPartFromSocket(SOCKET sc, int bytesNum, Cryptor& cryptor)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	string res(s);
	string rtn = cryptor.decryptRSA(s);
	return rtn;
}


// recieve data from socket according byteSize
// this is private function
char* Helper::getPartFromSocket(SOCKET sc, int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}

char* Helper::getPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{
		return "";
	}

	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	data[bytesNum] = 0;
	return data;
}


string Helper::getPaddedNumber(int num, int digits)
{
	std::ostringstream ostr; 
	ostr <<  std::setw(digits) << std::setfill('0') << num;
	return ostr.str();

}