#include "Cryptor.h"

#define KEYLENGTH 3072

CryptoPP::SHA256 Cryptor::_hash;


string Cryptor::hashStr(string input)
{
	string rtn;
	CryptoPP::StringSource s(input, true, new CryptoPP::HashFilter(_hash, new CryptoPP::HexEncoder(new CryptoPP::StringSink(rtn))));
	return rtn;
}


Cryptor::Cryptor()
{
	CryptoPP::InvertibleRSAFunction params;
	params.GenerateRandomWithKeySize(_rng, KEYLENGTH);
	_privateKey = new CryptoPP::RSA::PrivateKey(params);
	_publicKey = new CryptoPP::RSA::PublicKey(params);
	_friendPublicKey = new CryptoPP::RSA::PublicKey();
}

Cryptor::~Cryptor()
{
	delete _privateKey;
	delete _publicKey;
	delete _friendPublicKey;
}

char* Cryptor::encryptRSA(string input)
{

	try
	{
		vector<byte> plain;
		vector<byte> cipher;
		cipher.resize(384);
		plain.resize(input.size());

		for (int i = 0; i < input.size(); i++)
		{
			plain[i] = input[i];
		}


		CryptoPP::ArraySink cs(&cipher[0], cipher.size());
		CryptoPP::RSAES_PKCS1v15_Encryptor e(*_friendPublicKey);
		CryptoPP::ArraySource ss1(plain.data(), plain.size(), true, new CryptoPP::PK_EncryptorFilter(_rng, e, new CryptoPP::Redirector(cs)));
		cipher.resize(cs.TotalPutLength());
		char* rtn = new char[cipher.size()];
		for (int i = 0; i < cipher.size(); i++)
		{
			rtn[i] = cipher[i];
		}
		return rtn;
	}

	catch (exception& e)
	{
		cout << "Encryption error: " << e.what() << endl;
		system("pause");
	}
	
	
}


void Cryptor::initalize(CryptoPP::Integer n, CryptoPP::Integer e)
{
	_friendPublicKey->Initialize(n, e);
}

string Cryptor::decryptRSA(char* input)
{
	byte* byteInput = (byte*)input;
	string decipher;
	vector<byte> recover;
	recover.resize(384);
	CryptoPP::ArraySink rs(&recover[0], recover.size());
	CryptoPP::RSAES_PKCS1v15_Decryptor d(*_privateKey);
	CryptoPP::ArraySource ss2(byteInput, 384, true, new CryptoPP::PK_DecryptorFilter (_rng, d, new CryptoPP::Redirector(rs)));
	recover.resize(rs.TotalPutLength());

	decipher = string((char*)recover.data());
	return decipher;
}

void Cryptor::readPublicKey(string key)
{
	CryptoPP::StringSource stringSource(key, true);
	_friendPublicKey->BERDecode(stringSource);
	
}

void Cryptor::setFriendModulus(char* key)
{
	_friendPublicKey->SetModulus(CryptoPP::Integer(key));
}

char* Cryptor::getExponent()
{
	size_t size = _publicKey->GetPublicExponent().MinEncodedSize();
	vector<byte>* bytes = new vector<byte>();
	bytes->resize(size);
	_publicKey->GetPublicExponent().Encode(&(*bytes)[0], bytes->size());
	return (char*) &(*bytes)[0];

	
}

int Cryptor::getExponentLength()
{
	return _publicKey->GetPublicExponent().ByteCount();
}

int Cryptor::getModulusLength()
{
	return _publicKey->GetModulus().ByteCount();
}

char* Cryptor::getModulus()
{
	size_t size = _publicKey->GetModulus().MinEncodedSize();
	vector<byte>* bytes = new vector<byte>();
	bytes->resize(size);
	_publicKey->GetModulus().Encode(&(*bytes)[0], bytes->size());
	return (char*)&((*bytes)[0]);
}

void Cryptor::setFriendExponent(char* exp)
{
	_friendPublicKey->SetPublicExponent(CryptoPP::Integer(exp));
}

string Cryptor::encodePublicKey()
{
	string rtn;
	CryptoPP::StringSink strinkSink(rtn);
	_publicKey->BEREncode(strinkSink);
	return rtn;
}