#pragma once

#include <iostream>
#include <string>
#include "Helper.h"
#define ACTIVE 0
#define INACTIVE 1

typedef unsigned int Uint;

using namespace std;

class Stdafx
{
public:
	static string parseInt(int num);
	static string parseIntTo4Bytes(int number);
	static string parseIntToXBytes(int number, int bytes);
};