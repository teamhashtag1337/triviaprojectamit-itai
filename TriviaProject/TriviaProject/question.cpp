#include "Question.h"

Question::Question(int id, string question, string answer1, string answer2, string answer3, string answer4)
{
	srand(time(NULL));
	int correctInd = (rand() % 4);
	_id = id;
	_question = question;

	string tmp[] = { answer2, answer3, answer4 };
	int tmpInd = 0;

	for(int i = 0; i < 4; i++)
	{
		if (i == correctInd)
			_answers[i] = answer1;

		else
			_answers[i] = tmp[tmpInd++];
		
	}

	_correctAnswerIndex = correctInd;
}

string Question::getQuestion()
{
	return _question;
}

string* Question::getAnswers()
{
	return _answers;
}

int Question::getCorrectAnswerIndex()
{
	return _correctAnswerIndex + 1;
}

int Question::getId()
{
	return _id;
}