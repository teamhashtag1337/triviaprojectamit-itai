#include "Room.h"

Room::Room(int id, User* admin, string name, int maxUsers, int questionTime, int questionsNo)
{
	_id = id;
	_admin = admin;
	_name = name;
	_maxUsers = maxUsers;
	_questionTime = questionTime;
	_questionsNo = questionsNo;
	_users.push_back(admin); //Inserting the admin to the users list
	admin->setRoom(this);
}

string Room::getUsersAsString(vector<User*> usersList, User* excludeUser)
{
	string usersStr = "";
	for (auto iter = usersList.begin(); iter != usersList.end(); iter++) //For each user in the usersList
	{
		if (*iter != excludeUser)
			usersStr += (*iter)->getUserName() + "\n";
	}
	return usersStr;
}

void Room::sendMessage(string message)
{
	sendMessage(NULL, message);
}

void Room::sendMessage(User* excludeUser, string message)
{
	try
	{
		for (auto iter = _users.begin(); iter != _users.end(); iter++) //For each user in the users list
		{
			if (*iter != excludeUser)
				(*iter)->send(message);
		}
	}
	catch (...)
	{

	}
}

bool Room::joinRoom(User* user)
{
	if (_maxUsers == _users.size()) //If the room is ful
	{
		user->send("1101");
		return false;
	}
	_users.push_back(user);
	user->setRoom(this);
	user->send("1100" + Stdafx::parseInt(_questionsNo) + Stdafx::parseInt(_questionTime)); //Success message
	sendMessage(getUsersListMessage()); //Sending the updated users list after the user joined to all users
}

void Room::leaveRoom(User* user)
{
	
	auto it = find(_users.begin(), _users.end(), user);

	if (it != _users.end())
	{
		user->send("1120"); //Leave success
		_users.erase(it);
		sendMessage(user, getUsersListMessage()); //Sending the updated users list after the user left to all users


	}

	cout << "Left room\n\n";
}

int Room::closeRoom(User* user)
{
	if (user == _admin) //If the admin wants to close the server
	{
		sendMessage("116"); //Room closing
		for (auto iter = _users.begin(); iter != _users.end(); iter++) //For each user in the users list
		{
			if (*iter != user) //If the user isn't the admin
				(*iter)->clearRoom();
		}
		return _id;
	}
	else
		return -1;
}

vector<User*> Room::getUsers()
{
	return _users;
}

string Room::getUsersListMessage()
{
	cout << "Sending users list..\n\n";
	string message = "108";
	message += (_users.size() + '0');
	for (auto iter = _users.begin(); iter != _users.end(); iter++) //For each user in the user list
	{
		message += Stdafx::parseInt((*iter)->getUserName().length());
		message += (*iter)->getUserName();
	}
	return message;
}

int Room::getQuestionsNo()
{
	return _questionsNo;
}

int Room::getId()
{
	return _id;
}

string Room::getName()
{
	return _name;
}

int Room::getQuestionsTime()
{
	return _questionTime;
}